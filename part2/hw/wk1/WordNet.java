public class WordNet {

    // constructor takes the name of the two input files

    private LinearProbingHashST<String, Bag<Integer>> synsets;
    private LinearProbingHashST<Integer, String> synNames;
    private Digraph graph;
    private SAP sap;

    public WordNet(String synsetsFile, String hypernymsFile) {
        
        In synsFile = new In(synsetsFile);
        In hypersFile = new In(hypernymsFile);

        synsets = new LinearProbingHashST<String, Bag<Integer>>();
        synNames = new LinearProbingHashST<Integer, String>();
        Bag<Integer> synIds;
        while (!synsFile.isEmpty()) {
            String[] s = synsFile.readLine().trim().split(",");
            int id = Integer.parseInt(s[0]);
            synNames.put(id, s[1]);
            String[] synset = s[1].split("\\s");
            String gloss = s[2];
            for (int i = 0; i < synset.length; i++) {
                String syn = synset[i];
                synIds = synsets.get(syn);
                if (synIds == null) 
                    synIds = new Bag<Integer>();
                synIds.add(id);
                synsets.put(syn, synIds);
            }
        }
        graph = new Digraph(synNames.size());
        while (!hypersFile.isEmpty()) {
            String[] s = hypersFile.readLine().trim().split(",");
            int id = Integer.parseInt(s[0]);
            for (int i = 1; i < s.length; i++) {
                int hypernym = Integer.parseInt(s[i]);
                graph.addEdge(id, hypernym);
            }
        }

        //check graph for fallacies
        int rootsCount = 0, root = 0;
        for (int i = 0; i < graph.V(); i++) {
            if (!graph.adj(i).iterator().hasNext()) {
                rootsCount++;
                root = i;
            }
        }
        if (rootsCount != 1) throw new java.lang.IllegalArgumentException(String.format("Should have exactly one root, roots %d", rootsCount));
        DirectedCycle dc = new DirectedCycle(graph);
        if (dc.hasCycle()) throw new java.lang.IllegalArgumentException("Has a cycle");

        sap = new SAP(graph);

    }

    // the set of nouns (no duplicates), returned as an Iterable
    public Iterable<String> nouns() {
        
        return synsets.keys();
    }

    // is the word a WordNet noun?
    public boolean isNoun(String word) { 
        
        return synsets.contains(word);
    }

    // distance between nounA and nounB (defined below)
    public int distance(String nounA, String nounB) {
        
        if (!isNoun(nounA) || !isNoun(nounB)) {
            throw new java.lang.IllegalArgumentException("Not a noun");
        }
        return sap.length(synsets.get(nounA), synsets.get(nounB));
    }

    // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
    // in a shortest ancestral path (defined below)
    public String sap(String nounA, String nounB) {
        
        if (!isNoun(nounA) || !isNoun(nounB)) {
            throw new java.lang.IllegalArgumentException("Not a noun");
        }
        int commonAncestorId = sap.ancestor(synsets.get(nounA), synsets.get(nounB));
        return synNames.get(commonAncestorId);
    }

    // for unit testing of this class
    public static void main(String[] args) {

        WordNet wn = new WordNet(args[0], args[1]);
        while (!StdIn.isEmpty()) {
            String s1 = StdIn.readString();
            String s2 = StdIn.readString();
            try {
                int distance = wn.distance(s1, s2);
                String sap = wn.sap(s1, s2);
                StdOut.printf("distance = %d, sap = %s\n", distance, sap);
            }
            catch (java.lang.IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
            
        }
    }
    
}