import java.util.*;

public class SAP {
    
    // constructor takes a digraph (not necessarily a DAG)
    //private Digraph graph;

    private DeluxeBFS bfs;
    private int vertNum;
    private Iterable<Integer> cachedVs;
    private Iterable<Integer> cachedWs;
    private Ancestor cachedAncestor;
    
    private class Ancestor {

        private LinearProbingHashST<Integer, Integer> distsV;
        private LinearProbingHashST<Integer, Integer> distsW;
        private int vertex;
        private int length;

        public Ancestor(int v, int w) {

            distsV = new LinearProbingHashST<Integer, Integer>();
            distsW = new LinearProbingHashST<Integer, Integer>();
            if (v < 0 || v > vertNum || w < 0 || w > vertNum)
                throw new java.lang.IndexOutOfBoundsException("Illegal vertex");
            bfs.search(v);
            for (int x: bfs.getLatestTrace())
                distsV.put(x, bfs.distTo(x));
            bfs.search(w);
            for (int x: bfs.getLatestTrace())
                distsW.put(x, bfs.distTo(x));
            findBest();
        }

        public Ancestor(Iterable<Integer> vs, Iterable<Integer> ws) {

            distsV = new LinearProbingHashST<Integer, Integer>();
            distsW = new LinearProbingHashST<Integer, Integer>();
            for (int v: vs) {
                if (v < 0 || v > vertNum)
                    throw new java.lang.IndexOutOfBoundsException("Illegal vertex");
            }
            for (int w: ws) {
                if (w < 0 || w > vertNum)
                    throw new java.lang.IndexOutOfBoundsException("Illegal vertex");
            }
            bfs.search(vs);
            for (int x: bfs.getLatestTrace())
                distsV.put(x, bfs.distTo(x));
            bfs.search(ws);
            for (int x: bfs.getLatestTrace())
                distsW.put(x, bfs.distTo(x));
            findBest();
        }

        private void findBest() {

            int bestAncestor = -1, bestLength = Integer.MAX_VALUE;
            for (int x: bfs.getLatestTrace()) {
                if (distsV.contains(x) && distsW.contains(x) 
                    && bestLength > (distsV.get(x) + distsW.get(x))) {
                    bestLength = distsV.get(x) + distsW.get(x);
                    bestAncestor = x;
                }
            }
            vertex = bestAncestor;
            length = (bestLength != Integer.MAX_VALUE) ? bestLength : -1;
        }

        public int getVertex() {
            return vertex;
        }

        public int getLength() {
            return length;
        }
    }

    public SAP(Digraph G) {

        vertNum = G.V();
        bfs = new DeluxeBFS(new Digraph(G));
    }

    // length of shortest ancestral path between v and w; -1 if no such path
    public int length(int v, int w) {
        
        Ancestor a = new Ancestor(v, w);
        return a.getLength();
    }

    // // a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
    public int ancestor(int v, int w) {
            
        Ancestor a = new Ancestor(v, w);
        return a.getVertex();
    }

    // check if given items are already cached
    private boolean cacheIsValid(Iterable<Integer> vs, Iterable<Integer> ws) {

        if (cachedVs == null || cachedWs == null) return false;
        return (vs == cachedVs && ws == cachedWs);
    }

    // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
    public int length(Iterable<Integer> vs, Iterable<Integer> ws) {

        if (!cacheIsValid(vs, ws)) {
            //System.out.println("cache miss");
            cachedAncestor = new Ancestor(vs, ws);
            cachedVs = vs;
            cachedWs = ws;
        }
        return cachedAncestor.getLength();
    }

    // a common ancestor that participates in shortest ancestral path; -1 if no such path
    public int ancestor(Iterable<Integer> vs, Iterable<Integer> ws) {
        
        if (!cacheIsValid(vs, ws)) {
            cachedAncestor = new Ancestor(vs, ws);
            cachedVs = vs;
            cachedWs = ws;
        }
        return cachedAncestor.getVertex();
    }

    // for unit testing of this class (such as the one below)
    public static void main(String[] args) {

        In in = new In(args[0]);
        Digraph G = new Digraph(in);
        SAP sap = new SAP(G);
        while (!StdIn.isEmpty()) {
            int v = StdIn.readInt();
            int w = StdIn.readInt();
            int length   = sap.length(v, w);
            int ancestor = sap.ancestor(v, w);
            StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
        }
    }
}