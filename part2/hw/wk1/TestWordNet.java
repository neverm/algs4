import static org.junit.Assert.*;

public class TestWordNet {

    private WordNet init1;
    public TestWordNet() {
        init1 = new WordNet("test/synsets.txt", "test/hypernyms.txt");
    }

    public void TestCreation() {

        //double delta = 0.0001;
        ST<String, Boolean> syns = new ST<String, Boolean>();
        syns.put("first", true);
        syns.put("second", true);
        syns.put("test", true);
        syns.put("id_3", true);
        syns.put("test", true);
        StdOut.println("--- TEST Creation ---");
        for (String syn: init1.nouns()) {
            assertTrue(syns.contains(syn));
        }
    }

    public void TestIsNoun() {

        StdOut.println("--- TEST isNoun ---");
        assertTrue(init1.isNoun("test"));
        assertFalse(init1.isNoun("tasdasdasdest"));
    }

    public void TestCycle() {

        StdOut.println("--- TEST Cycles ---");
        try {
            WordNet wn = new WordNet("test/synsets.txt", "test/hypernyns_cycle.txt");
            assertTrue(false);
        }
        catch (java.lang.IllegalArgumentException e) {
            assertTrue(true);
        }
    }

    public void TestRoots() {

        StdOut.println("--- TEST multiple roots ---");
        try {
            WordNet wn = new WordNet("test/synsets.txt", "test/hypernyms_two_roots.txt");
            assertTrue(false);
        }
        catch (java.lang.IllegalArgumentException e) {
            assertTrue(true);
        }
    }



    public static  void main(String[] args) {

        StdOut.printf("Start...\n\n");

        TestWordNet tester = new TestWordNet();

        try {
            tester.TestCreation();
            tester.TestIsNoun();
            tester.TestCycle();
            tester.TestRoots();
        }

        catch (Exception e) {
            StdOut.println(e.toString());
        }

        StdOut.printf("\n\n...finish.\n");
    }

}