public  class Outcast {

    private WordNet wn;
    // constructor takes a WordNet object
    public Outcast(WordNet wordnet) {

        wn = wordnet;
    }

    // given an array of WordNet nouns, return an outcast
    public String outcast(String[] nouns) {
        
        int m = nouns.length;
        int[][] lengths = new int[m][m];
        String worstNoun = nouns[0];
        int maxLen = 0;
        for (int i = 0; i < m; i++) {
            for (int j = i+1; j < m; j++) {
                if (j == i) 
                	lengths[i][j] = 0;
                else {
                	lengths[i][j] = wn.distance(nouns[i], nouns[j]);
                	lengths[j][i] = lengths[i][j];
                }
            }
        }
        for (int i = 0; i < m; i++) {
        	int totalLen = 0;
            for (int j = 0; j < m; j++) {
            	totalLen += lengths[i][j];
            }
	        if (totalLen > maxLen) {
                maxLen = totalLen;
                worstNoun = nouns[i];
            }
        }
        return worstNoun;
    }

    // for unit testing of this class (such as the one below)
    public static void main(String[] args) {

        Stopwatch swCreation = new Stopwatch();
        WordNet wordnet = new WordNet(args[0], args[1]);
        System.out.printf("creation time: %s\n", swCreation.elapsedTime());
        Stopwatch sw = new Stopwatch();
        Outcast outcast = new Outcast(wordnet);
        for (int t = 2; t < args.length; t++) {
            In in = new In(args[t]);
            String[] nouns = in.readAllStrings();
            StdOut.println(args[t] + ": " + outcast.outcast(nouns));
        }
        System.out.println(sw.elapsedTime()); 
    }
}