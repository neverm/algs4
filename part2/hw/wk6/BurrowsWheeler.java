public class BurrowsWheeler {

    // get last character of suffix with given offset
    private static char charAt(String s, int offset) {
        int lastIndex = (offset + (s.length()-1)) %  s.length();
        return s.charAt(lastIndex);
    }

    // apply Burrows-Wheeler encoding, reading from standard input and writing to standard output
    public static void encode() {

        String input = BinaryStdIn.readString();
        BinaryStdIn.close();
        CircularSuffixArray csa = new CircularSuffixArray(input);
        int first = -1;
        for (int i = 0; i < csa.length(); ++i)
            if (csa.index(i) == 0) first = i;
        BinaryStdOut.write(first);
        for (int i = 0; i < csa.length(); ++i)
            // put to the ith position the last character of suffix csa.index(i)
            BinaryStdOut.write(charAt(input, csa.index(i)));
        BinaryStdOut.close();
    }

    // apply Burrows-Wheeler decoding, reading from standard input and writing to standard output
    public static void decode() {

        int first = BinaryStdIn.readInt();
        char[] t = BinaryStdIn.readString().toCharArray();
        BinaryStdIn.close();
        int R = 256;
        int[] counts = new int[R+1];
        int[] next = new int[t.length];
        // calculate how many times character c occur in text
        for (int i = 0; i < t.length; i++)
            counts[t[i]+1]++;
        // sum up count terms, so that counts[c] shows how many characters less than c are in text
        // basically this is index counting sort, so counts[c] shows position of first occurence of c in sorted array
        for (int c = 1; c < counts.length; c++)
            counts[c] = counts[c] + counts[c-1];
        // setup next array from sorted suffices
        for (int i = 0; i < t.length; i++) {
            next[counts[t[i]]] = i;
            counts[t[i]]++;
        }
        // t[first] is actually the LAST character, but t[next[first]] is first
        // that's because t is the last character of suffix
        int current = next[first];
        for (int i = 0; i < t.length; i++) {
            BinaryStdOut.write(t[current]);
            current = next[current];   
        }
        // BinaryStdOut.write(t[current]);
        BinaryStdOut.close();

    }

    // if args[0] is '-', apply Burrows-Wheeler encoding
    // if args[0] is '+', apply Burrows-Wheeler decoding
    public static void main(String[] args) {

        if (args.length < 1)
            throw new IllegalArgumentException("Too few arguments. Use arg - for encoding, + for decoding");
        BurrowsWheeler bw = new BurrowsWheeler();
        if (args[0].equals("-"))
            bw.encode();
        else if (args[0].equals("+"))
            bw.decode();
        else
            throw new IllegalArgumentException("Wrong argument. Use arg - for encoding, + for decoding");
    }
}