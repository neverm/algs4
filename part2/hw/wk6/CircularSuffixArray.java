public class CircularSuffixArray {

    private static final int R = 256;
    private static final int CUTOFF = 13;
    private int[] index;
    private int l;
    private String s;
    // circular suffix array of s
    public CircularSuffixArray(String str) {

        l = str.length();
        index = new int[l];
        s = str;
        for (int i = 0; i < l; i++)
            index[i] = i;
        sort(index);
    }

    // get d-th character of i-th suffix
    private int ch(int i, int d) {
        
        if (d > s.length()) return -1;
        return s.charAt((i+d) % s.length());
    }

    private void sort(int[] a) {

        int[] aux = new int[a.length];
        sort(a, aux, 0, a.length-1, 0);
    }

    private void sort(int[] a, int[] aux, int lo, int hi, int d) {

        if (lo >= hi) return;
        // System.out.printf("Sort called: (%d, %d) at level %d\n", lo, hi, d);
        if (lo + CUTOFF >= hi) {
            insertionSort(a, aux, lo, hi, d);
            return;
        }
        int[] count = new int[R+2];
        for (int i = lo; i <= hi; i++)
            count[ch(a[i], d)+2]++;
        for (int r = 0; r < R+1; r++)
            count[r+1] += count[r];
        for (int i = lo; i <= hi; i++)
            aux[count[ch(a[i], d)+1]++] = a[i];
        for (int i = lo; i <= hi; i++)
            a[i] = aux[i-lo];

        for (int r = 0; r < R; r++)
            sort(a, aux, lo + count[r], lo + count[r+1] - 1, d+1);

    }

    private void insertionSort(int[] a, int[] aux, int lo, int hi, int d) {

        if (lo >= hi) return;
        // System.out.println("Insertion called!");
        for (int i = lo; i < hi; i++) {
            for (int j = i+1; j > lo && less(a[j], a[j-1], d); j--) {
                int tmp = a[j];
                a[j] = a[j-1];
                a[j-1] = tmp;
            }
        }
    }

    private boolean less(int x, int y, int d) {

        for (int i = d; i < index.length; i++) {
            if (ch(x, i) < ch(y, i)) return true;
            else if (ch(x, i) > ch(y, i)) return false;
        }
        return false;
    }

    // length of s
    public int length() {
        return l;
    }
    // returns index of ith sorted suffix
    public int index(int i) {

        return index[i];
    }

    public static void main(String[] args) {
        
        CircularSuffixArray csa = new CircularSuffixArray("ABRACADABRA!");
        for (int i = 0; i < csa.l; i++) {
            
            StdOut.println(csa.index[i]);
        }
    }
    
}