public class MoveToFront {

    // apply move-to-front encoding, reading from standard input and writing to standard output
    public static void encode() {

        char[] alphabet = new char[256];
        for (int i = 0; i < alphabet.length; i++)
            alphabet[i] = (char) i;
        String s = BinaryStdIn.readString();
        BinaryStdIn.close();
        char[] input = s.toCharArray();
        for (int i = 0; i < input.length; i++) {
            char j;
            for (j = 0; j < alphabet.length; j++)
                if (alphabet[j] == input[i]) break;
            BinaryStdOut.write(j);
            while (j > 0) {
                char tmp = alphabet[j];
                alphabet[j] = alphabet[j-1];
                alphabet[j-1] = tmp;
                j--;
            }
        }
        BinaryStdOut.close();
    }

    // apply move-to-front decoding, reading from standard input and writing to standard output
    public static void decode() {

        char[] alphabet = new char[256];
        for (int i = 0; i < alphabet.length; i++)
            alphabet[i] = (char) i;
        String s = BinaryStdIn.readString();
        char[] input = s.toCharArray();
        for (int i = 0; i < input.length; i++) {
            // input[i] is POSITION in alphabet created so far, not actual char
            int j = input[i];
            BinaryStdOut.write(alphabet[j]);
            while (j > 0) {
                char tmp = alphabet[j];
                alphabet[j] = alphabet[j-1];
                alphabet[j-1] = tmp;
                j--;
            }
        }
        BinaryStdOut.close();
        
    }

    // if args[0] is '-', apply move-to-front encoding
    // if args[0] is '+', apply move-to-front decoding
    public static void main(String[] args) {

        if (args.length < 1)
            throw new IllegalArgumentException("Too few arguments. Use arg - for encoding, + for decoding");
        MoveToFront mf = new MoveToFront();
        if (args[0].equals("-"))
            mf.encode();
        else if (args[0].equals("+"))
            mf.decode();
        else
            throw new IllegalArgumentException("Wrong argument. Use arg - for encoding, + for decoding");

    }
}