import static org.junit.Assert.*;

class TestBaseballElimination {

	public void TestCreation() {

		StdOut.println("--- TEST Creation ---");
		assertTrue(true);
		assertFalse(false);
		assertEquals(1, 1);

	}

	public void testNum() {

		BaseballElimination be = new BaseballElimination("test/teams10.txt");
		StdOut.println("--- TEST Num ---");
		assertEquals(be.numberOfTeams(), 10);
	}

	public void testWins() {

		BaseballElimination be = new BaseballElimination("test/teams10.txt");
		StdOut.println("--- TEST Wins ---");
		assertEquals(be.wins("Atlanta"), 0);
		assertEquals(be.wins("Indiana"), 32);
	}

	public void testLosses() {

		BaseballElimination be = new BaseballElimination("test/teams10.txt");
		StdOut.println("--- TEST Losses ---");
		assertEquals(be.losses("Indiana"), 31);
		assertEquals(be.losses("Denver"), 0);
	}

	public void testRemaining() {

		BaseballElimination be = new BaseballElimination("test/teams10.txt");
		StdOut.println("--- TEST Remaining ---");
		assertEquals(be.remaining("Indiana"), 0);
		assertEquals(be.remaining("Atlanta"), 63);
	}

	public void testAgainst() {

		BaseballElimination be = new BaseballElimination("test/teams10.txt");
		StdOut.println("--- TEST against ---");
		assertEquals(be.against("Atlanta", "Atlanta"), 0);
		assertEquals(be.against("Atlanta", "Boston"), 9);
	}

	public static  void main(String[] args) {

		StdOut.printf("Start...\n\n");

		TestBaseballElimination tester = new TestBaseballElimination();

		try {
		    tester.testNum();
		    tester.testWins();
		    tester.testLosses();
		    tester.testRemaining();
		    tester.testAgainst();
		}

		catch (java.lang.AssertionError e) {
		    StdOut.println(e.toString());
		}

		StdOut.printf("\n\n...finish.\n");
    }
}