public class BaseballElimination {

    private ST<String, Integer> teams;
    private int[] w, l, r;
    private int[][] g;
    private int N;
    private String[] teamNames;
    private BaseballFlow flow;
    
    // create a baseball division from given filename in format specified below
    public BaseballElimination(String filename) {

        In file = new In(filename);
        N = Integer.parseInt(file.readLine().trim());
        w = new int[N];
        l = new int[N];
        r = new int[N];
        g = new int[N][N];
        teamNames = new String[N];
        teams = new ST<String, Integer>();
        int teamIndex = 0;
        while (!file.isEmpty()) {
            String[] teamData = file.readLine().trim().split("\\s+");
            teams.put(teamData[0].trim(), teamIndex);
            teamNames[teamIndex] = teamData[0].trim();
            w[teamIndex] = Integer.parseInt(teamData[1]);
            l[teamIndex] = Integer.parseInt(teamData[2]);
            r[teamIndex] = Integer.parseInt(teamData[3]);
            for (int against = 0; against < N; against++) {
                g[teamIndex][against] = Integer.parseInt(teamData[4+against]); // 4 - starting index of games data
            }
            teamIndex++;
        }
        flow = new BaseballFlow();
    }

    private class BaseballFlow {

        private String currentTeam;
        private FordFulkerson ff;

        public BaseballFlow() {

        }

        // set team to given and build appropriate datastructures with respect to it
        public void setTeam(String team) {

            if (currentTeam == team)
                return;
            if (!teams.contains(team))
                throw new java.lang.IllegalArgumentException(String.format("No team found: %s", team));
            currentTeam = team;
            int teamIndex = teams.get(team);
            //build flow newtwork and ford fulkerson
            // number of teams - 1, plus number of games between all pairs of (teams - 1) plus 2 for src and target
            int totalVertices = N + N*N + 2;
            int src = totalVertices - 2;
            int target = totalVertices - 1;
            int possibleWins = w[teamIndex] + r[teamIndex];
            FlowNetwork network = new FlowNetwork(totalVertices);
            // setup edges from team vertices to target (vertex numbers from 0 to N-1)
            for (int i = 0; i < N; i++) {
                if (i == teamIndex) continue;
                network.addEdge(new FlowEdge(i, target, possibleWins - w[i]));
            }
            // setup edges from start to games and from games to teams
            for (int i = 0; i < N; i++) {
                for (int j = i+1; j < N; j++) {
                    if (i == teamIndex || j == teamIndex) continue;
                    network.addEdge(new FlowEdge(src, mapToGame(i, j), g[i][j]));
                    network.addEdge(new FlowEdge(mapToGame(i, j), i, Integer.MAX_VALUE));
                    network.addEdge(new FlowEdge(mapToGame(i, j), j, Integer.MAX_VALUE));
                }
            }
            ff = new FordFulkerson(network, src, target);
        }

        // map two team vertices to appropriate game vertex address
        private int mapToGame(int v, int w) {
            return N + v*N + w;
        }

        // value of baseball flow
        public int value() {

            if (currentTeam == null) throw new java.lang.IllegalArgumentException("No team is set");
            return (int) ff.value();
        }

        // checks whether given team is in cut (i.e. is it in the certificate of elimination)
        public boolean inCut(String team) {

            if (currentTeam == null) throw new java.lang.IllegalArgumentException("No team is set");
            return ff.inCut(teams.get(team));
        }

        public boolean inCut(int teamIndex) {

            if (currentTeam == null) throw new java.lang.IllegalArgumentException("No team is set");
            return ff.inCut(teamIndex);
        }
    }

    // number of teams
    public int numberOfTeams() {
        return N;
    }
    
    // all teams
    public Iterable<String> teams() {
        return teams.keys();
    }
    
    // number of wins for given team
    public int wins(String team) {

        if (!teams.contains(team)) throw new java.lang.IllegalArgumentException(String.format("no such team: %s", team));
        return w[teams.get(team)];
    }
    
    // number of losses for given team
    public int losses(String team) {

        if (!teams.contains(team)) throw new java.lang.IllegalArgumentException(String.format("no such team: %s", team));
        return l[teams.get(team)];
    }
    
    // number of remaining games for given team
    public int remaining(String team) {

        if (!teams.contains(team)) throw new java.lang.IllegalArgumentException(String.format("no such team: %s", team));
        return r[teams.get(team)];
    }

    // number of remaining games between team1 and team2
    public int against(String team1, String team2) {

        if (!teams.contains(team1)) throw new java.lang.IllegalArgumentException(String.format("no such team: %s", team1));
        if (!teams.contains(team2)) throw new java.lang.IllegalArgumentException(String.format("no such team: %s", team2));
        return g[teams.get(team1)][teams.get(team2)];
    }

    // is given team eliminated?
    public boolean isEliminated(String team) {

        if (!teams.contains(team)) throw new java.lang.IllegalArgumentException(String.format("no such team: %s", team));
        if (isTriviallyEliminated(team)) 
            return true;
        int teamIndex = teams.get(team);
        flow.setTeam(team);
        for (int i = 0; i < N; i++) {
            if (i == teamIndex) continue;
            if (flow.inCut(i)) return true;
        }
        return false;
    }

    private boolean isTriviallyEliminated(String team) {
        
        int teamIndex = teams.get(team);
        int possibleWins = w[teamIndex] + r[teamIndex];
        for (int i = 0; i < N; i++) {
            if (possibleWins < w[i])
                return true;
        }
        return false;
    }

    // subset R of teams that eliminates given team; null if not eliminated
    public Iterable<String> certificateOfElimination(String team) {

        if (!teams.contains(team)) throw new java.lang.IllegalArgumentException(String.format("no such team: %s", team));
        Stack<String> st = new Stack<String>();
        int teamIndex = teams.get(team);
        if (isTriviallyEliminated(team)) {
            int possibleWins = w[teamIndex] + r[teamIndex];
            for (int i = 0; i < N; i++) {
                if (possibleWins < w[i])
                    st.push(teamNames[i]);
            }
        }
        else {
            flow.setTeam(team);
            for (int i = 0; i < N; i++) {
                if (i == teamIndex) continue;
                if (flow.inCut(i))
                    st.push(teamNames[i]);
            }
        }
        if (st.size() > 0)
            return st;
        else return null;
    }

    public static void main(String[] args) {
        
        BaseballElimination division = new BaseballElimination(args[0]);
        for (String team : division.teams()) {
            if (division.isEliminated(team)) {
                StdOut.print(team + " is eliminated by the subset R = { ");
                for (String t : division.certificateOfElimination(team))
                    StdOut.print(t + " ");
                StdOut.println("}");
            }
            else {
                StdOut.println(team + " is not eliminated");
            }
        }
    }
}