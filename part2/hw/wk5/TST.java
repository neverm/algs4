public class TST {

    private Node root;
    private int size;
    private String lastPrefix;
    private Node lastPrefixNode;

    private class Node {

        public char ch;
        public boolean endsString;
        public Node left, mid, right;
        public Node(char c) {
            ch = c;
            endsString = false;
        }
    }
    public TST() {

        size = 0;
    }

    public void add(String s) {

        root = put(root, s, 0);
    }

    private Node put(Node x, String s, int d) {

        // put to the root
        if (x == null) x = new Node(s.charAt(d));
        if (s.charAt(d) < x.ch)
            x.left = put(x.left, s, d);
        else if (s.charAt(d) > x.ch)
            x.right = put(x.right, s, d);
        else { // (s.charAt(d) == x.ch) {
            if (s.length() == d+1) {
                if (!x.endsString)
                    size +=1;
                x.endsString = true;
            }
            else
                x.mid = put(x.mid, s, d+1);
        }
        return x;
    }

    public boolean contains(String s) {

        Node found = get(root, s, 0);
        return (found != null && found.endsString);
    }

    private Node get(Node x, String s, int d) {

        if (x == null || d > s.length()) return null;
        if (s.charAt(d) < x.ch)
            return get(x.left, s, d);
        if (s.charAt(d) > x.ch)
            return get(x.right, s, d);
        else { // (s.charAt(d) == x.ch) {
            if (s.length() == d+1)
                return x;
            else
                return get(x.mid, s, d+1);
        }
    }

    public int size() {
        return size;
    }

    public boolean hasPrefixSlow(String prefix) {

        Node found = get(root, prefix, 0);
        return (found != null);
    }

    public boolean hasPrefix(String prefix) {

        // Node found;
        if (prefix.substring(0, prefix.length()-1).equals(lastPrefix)) {
            // 2 because we want to revisit cached node with prelast character,
            // then he will send us in right direction
            lastPrefixNode = get(lastPrefixNode, prefix, prefix.length()-2); 
        }
        else
            lastPrefixNode = get(root, prefix, 0);
        lastPrefix = prefix;
        // lastPrefixNode = found;
        return (lastPrefixNode != null);
    }


    public static void main(String[] args) {
        
        TST tst = new TST();

        tst.add("a");
        tst.add("ul");
        tst.add("ula");
        tst.add("ulalala");
        System.out.println(tst.hasPrefix("u"));   
        System.out.println(tst.hasPrefix("ul"));   
        System.out.println(tst.hasPrefix("ula"));   
        System.out.println(tst.hasPrefix("ulal"));   
        System.out.println(tst.hasPrefix("ulala"));   
    }
}