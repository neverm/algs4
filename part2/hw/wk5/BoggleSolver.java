import java.util.*;

public class BoggleSolver {
    
    private HashSet<String> validWords;
    private MyTrieST dict;
    // private TST dict;
    private int M, N;
    private Object[] neighbors;

    // Initializes the data structure using the given array of strings as the dictionary.
    // (You can assume each word in the dictionary contains only the uppercase letters A through Z.)
    public BoggleSolver(String[] dictionary) {

        dict = new MyTrieST();
        // dict = new TST();
        // StdRandom.shuffle(dictionary);
        for (String s: dictionary) {
            // dict.add(s);
            dict.put(s, 1);
            // System.out.println(dict.get(s));
        }
    }

    private int getCellNum(int i, int j) {
        if (i < 0 || j < 0 || i >= M || j >= N)
            throw new java.lang.ArrayIndexOutOfBoundsException(String.format("Invalid indices: i=%d, j=%d", i, j));
        // System.out.printf("getCellNum for i=%d, j=%d, result is %d\n", i, j, i * N + j);
        return i * N + j;
    }

    private int getCellRow(int cellNum) {

        // System.out.printf("getCellRow for %d, result is %d\n", cellNum, (cellNum - cellNum % N) / M);
        if (N == 1)
            return cellNum;
        else
            return cellNum / N;
    }

    private int getCellCol(int cellNum) {

        // System.out.printf("getCellCol for %d, result is %d\n", cellNum, cellNum - getCellRow(cellNum) * N);
        if (M == 1) return cellNum;
        else 
            return cellNum % N;
    }

    private void calcNeighbors() {
        
        for (int cellNum = 0; cellNum < N*M; cellNum++) {
            Bag<Integer> nb = new Bag<Integer>();
            int i = getCellRow(cellNum);
            int j = getCellCol(cellNum);
            // System.out.printf("finding nbors for %d, i=%d, j=%d\n", cellNum, i, j);
            if (i > 0) {
                nb.add(getCellNum(i-1, j));
                if (j > 0)
                    nb.add(getCellNum(i-1, j-1));
                if (j < N-1)
                    nb.add(getCellNum(i-1, j+1));
            }
            if (i < M-1) {
                nb.add(getCellNum(i+1, j));
                if (j > 0)
                    nb.add(getCellNum(i+1, j-1));
                if (j < N-1)
                    nb.add(getCellNum(i+1, j+1));
            }
            if (j > 0)
                nb.add(getCellNum(i, j-1));
            if (j < N-1)
                nb.add(getCellNum(i, j+1)); 
            neighbors[cellNum] = nb;
        }
    }

    private Bag<Integer> getNeighbors(int cellNum) {

        return (Bag<Integer>) neighbors[cellNum];
    }

    // Get all valid words that start from cell (i,j) 
    private void getValidWords(int cell, String s, int[] visited, BoggleBoard board) {

        // get all valid words that start at s
        visited[cell] = 1;
        char cellLetter = board.getLetter(getCellRow(cell), getCellCol(cell));
        String currentWord;
        if (cellLetter == 'Q')
            currentWord = s + "QU";
        else 
            currentWord = s + cellLetter;
        if (currentWord.length() > 2 && dict.contains(currentWord))
            validWords.add(currentWord);
        boolean stopHere = (!dict.hasPrefix(currentWord));
        if (!stopHere) {
            for (int neighbor: getNeighbors(cell)) {
                if (visited[neighbor] == 1) continue;
                getValidWords(neighbor, currentWord, visited, board);
            }
        }
        visited[cell] = 0;
    }

    // Returns the set of all valid words in the given Boggle board, as an Iterable.
    public Iterable<String> getAllValidWords(BoggleBoard board) {

        // for all cells in board:
        //  get all valid words from the cell
        //  add to total words collection
        validWords = new HashSet<String>();
        N = board.cols();
        M = board.rows();
        neighbors = new Object[N*M];
        calcNeighbors();
        int[] visited = new int[N*M];
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                getValidWords(getCellNum(i, j), "", visited, board);
            }
        }
        return validWords;
    }

    // Returns the score of the given word if it is in the dictionary, zero otherwise.
    // (You can assume the word contains only the uppercase letters A through Z.)
    public int scoreOf(String word) {

        if (word.length() <= 2) return 0;
        if (!dict.contains(word)) return 0;
        switch (word.length()) {
            case 3: return 1;
            case 4: return 1; 
            case 5: return 2; 
            case 6: return 3; 
            case 7: return 5;
            default: return 11;
        }
    }

    public static void main(String[] args)
    {
        In in = new In(args[0]);
        String[] dictionary = in.readAllStrings();
        Stopwatch init = new Stopwatch();
        BoggleSolver solver = new BoggleSolver(dictionary);
        BoggleBoard board = new BoggleBoard(args[1]);
        System.out.printf("Init time: %.5f\n", init.elapsedTime());
        int numRuns = 5000;
        Stopwatch sw = new Stopwatch();
        int score = 0;
        for (int i = 0; i < numRuns; i++) {
            for (String word : solver.getAllValidWords(board))
            {
                // StdOut.println(word);
                score += solver.scoreOf(word);
            }
        }
        StdOut.printf("%d runs took %.5f seconds\n", numRuns, sw.elapsedTime());
        StdOut.println("Score = " + score);
    }
}