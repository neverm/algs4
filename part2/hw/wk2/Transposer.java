// test performance of transpose

class Transposer {

	public static void main(String[] args) {
		
		int w = 2;
		int h = 8000000;
		double[][] a = new double[w][h];

		for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				a[i][j] = StdRandom.random();
			}
		}

		double sum = 0;
		Stopwatch s1 = new Stopwatch();
		for (int c = 0; c < 100; c++) {
			for (int i = 0; i < w; i++) {
				for (int j = 0; j < h; j++) {
					sum += a[i][j];
				}
			}
		}
		System.out.println(s1.elapsedTime());

		sum = 0;
		Stopwatch s2 = new Stopwatch();
		for (int c = 0; c < 100; c++) {
			for (int j = 0; j < h; j++) {
				for (int i = 0; i < w; i++) {		
					sum += a[i][j];
				}
			}
		}
		System.out.println(s2.elapsedTime());
	}
	
}