import java.awt.Color;

public class SeamCarver {

    private Picture pic;
    private int[][] energies, grid;
    private boolean isChanged, isTransposed;
    private int w, h;

    
    private final int MAX_ENERGY = 195075;

    private class PathsBuilder {

        private int[] dist;
        private byte[][] edgeTo;
        private final byte NO_EDGE = 127;

        public PathsBuilder() {

            dist = new int[w];
            edgeTo = new byte[w][h];
            buildPaths();
        }

        public void buildPaths() {

            int[][] distTo = new int[w][h];
            for (int y = 0; y < h; y++) {
                for (int x = 0; x < w; x++) {
                    if (y == 0) {
                        distTo[x][y] = energies[x][y];
                        edgeTo[x][y] = NO_EDGE;
                        continue;
                    }
                    int bestPath = distTo[x][y-1];
                    byte neighbor = 0;
                    if (x > 0 && bestPath >= distTo[x-1][y-1]) {
                        bestPath = distTo[x-1][y-1];
                        neighbor = -1;
                    }
                    if (x < w-1 && bestPath > distTo[x+1][y-1]) {
                        bestPath = distTo[x+1][y-1];
                        neighbor = 1;
                    }
                    distTo[x][y] = energies[x][y] + distTo[x+neighbor][y-1];
                    edgeTo[x][y] = neighbor;
                }
            }
            for (int x = 0; x < w; x++)
                dist[x] = distTo[x][h-1];
        }

        // Get shortest distance to x-th pixel in bottom row
        public int getDist(int x) {

            return dist[x];
        }

        // get direction to "cheapest" pixel at top
        public byte getEdge(int x, int y) {

            return edgeTo[x][y];
        }
    }
    

    public SeamCarver(Picture picture) {

        pic = picture;
        w = pic.width();
        h = pic.height();
        isTransposed = false;
        isChanged = false;
        energies = new int[w][h];
        grid = new int[w][h];

        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                grid[x][y] = pic.get(x, y).getRGB();
            }
        }
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                energies[x][y] = (int) energy(x, y);
            }
        }
    }

    
    // current picture
    public Picture picture() {

        if (isChanged) {
            if (isTransposed)
                transpose();
            Picture p = new Picture(w, h);
            for (int y = 0; y < h; y++) {
                for (int x = 0; x < w; x++) {
                    p.set(x, y, new Color(grid[x][y]));
                }
            }
            pic = p;
            isChanged = false;
        }
        return pic;
    } 
                   
    public int width() { return w; }
                
    public int height() { return h; }           

    private int getRed(int x, int y) { return new Color(grid[x][y]).getRed(); }

    private int getBlue(int x, int y) { return new Color(grid[x][y]).getBlue(); }          

    private int getGreen(int x, int y) { return new Color(grid[x][y]).getGreen(); }

    // get squared difference between two color values
    private double getSqrDiff(int u, int w) { return (u - w) * (u - w); }         

    // energy of pixel at column x and row y in current picture
    public double energy(int x, int y) {

        if (x < 0 || x >= w || y < 0 || y >= h) {
            throw new java.lang.IndexOutOfBoundsException("Wrong coordinate");
        }
        if (x == 0 || x == w - 1 || y == 0 || y == h -1)
            return MAX_ENERGY;

        double hRed = getSqrDiff(getRed(x+1, y), getRed(x-1, y));
        double hBlue = getSqrDiff(getBlue(x+1, y), getBlue(x-1, y));
        double hGreen = getSqrDiff(getGreen(x+1, y), getGreen(x-1, y));
        double vRed = getSqrDiff(getRed(x, y-1), getRed(x, y+1));
        double vBlue = getSqrDiff(getBlue(x, y-1), getBlue(x, y+1));
        double vGreen = getSqrDiff(getGreen(x, y-1), getGreen(x, y+1));
        return hRed + hBlue + hGreen + vRed + vBlue + vGreen;
    }

    // transpose current energy and grid
    private void transpose() {

        int[][] energiesTrans = new int[h][w];
        int[][] gridTrans = new int[h][w];
        for (int x = 0; x < w; x++) {
            for (int y = 0; y < h; y++) {
                energiesTrans[y][x] = energies[x][y];
                gridTrans[y][x] = grid[x][y];
            }
        }
        energies = energiesTrans;
        grid = gridTrans;
        //swap width and height
        int temp = h;
        h = w;
        w = temp;
    }

    // sequence of indices for horizontal seam in current picture
    public int[] findHorizontalSeam() {

        transpose();
        int[] seam = findVerticalSeam();
        transpose();
        return seam;
    }        

    // sequence of indices for vertical   seam in current picture
    public int[] findVerticalSeam() {

        int[] seam = new int[h];
        int best = Integer.MAX_VALUE;
        int seamIndex = -1;
        PathsBuilder paths = new PathsBuilder();
        // find the lowest distance path index
        for (int x = 0; x < w; x++) {
            if (paths.getDist(x) < best) {
                best = paths.getDist(x);
                seamIndex = x;
            }
        }
        // recreate path by following edges
        for (int l = seam.length-1; l >= 0; l--) {
            seam[l] = seamIndex;
            seamIndex = seamIndex + paths.getEdge(seamIndex, l); // move to parent by adding edge to current index
        }
        return seam;
    }

    // remove horizontal seam from current picture
    public void removeHorizontalSeam(int[] a) {

        transpose();
        removeVerticalSeam(a);
        transpose();
    }  

    // remove vertical   seam from current picture
    public void removeVerticalSeam(int[] a) {

        isChanged = true;
        if (a.length != h)
            throw new java.lang.IllegalArgumentException(String.format("Wrong Vertical size: given %d, should be %d", a.length, h));
        

        w = w - 1;
        for (int y = 0; y < h; y++) {
            for (int x = a[y]; x < w; x++) { // start from removed pixel
                // shift all pixels on the place of removed
                grid[x][y] = grid[x+1][y];
            }
        }
        for (int x = 0; x < w; x++) {
            for (int y = 0; y < h; y++) {
                energies[x][y] = (int) energy(x, y);
            }
        }
    }

    public static void main(String[] args) {
        
        System.out.println("Initializing");
        SeamCarver sc = new SeamCarver(new Picture("medium.png"));
        
        //int[] seam = sc.findHorizontalSeam();
        Stopwatch sw = new Stopwatch();
        System.out.println("Finding seams");
        int[] seam;
        for (int i = 0; i < 25; i++) {
            sc.removeVerticalSeam(sc.findVerticalSeam());
            sc.removeHorizontalSeam(sc.findHorizontalSeam());
        }
        System.out.println(sw.elapsedTime());
        //printArr(sc.energies);
        //sc.removeVerticalSeam(seam);
        //printArr(sc.energies);
    }
}