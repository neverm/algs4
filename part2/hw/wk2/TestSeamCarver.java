import static org.junit.Assert.*;

class TestSeamCarver {

	public void TestCreation() {

		StdOut.println("--- TEST Creation ---");
		assertTrue(true);
		assertFalse(false);
		assertEquals(1, 1);

	}

	public void TestEnergy() {

		StdOut.println("--- TEST Energy ---");
		double epsilon = 0.00001;
		Picture pic = new Picture("test/3x7.png");
		SeamCarver sc = new SeamCarver(pic);
		assertEquals(195075, sc.energy(0, 0), epsilon);
		assertEquals(86627, sc.energy(1, 1), epsilon);
		assertEquals(195075, sc.energy(sc.width()-1, sc.height()-1), epsilon);
	}

	public void TestFindVertivalSeam() {

		StdOut.println("--- TEST FindVerticalSeam ---");
		Picture pic = new Picture("test/3x7.png");
		SeamCarver sc = new SeamCarver(pic);
		int[] seam = new int[] {0, 1, 1, 1, 1, 1, 0};
		int[] seamFound = sc.findVerticalSeam();
		double total = 0;
		assertArrayEquals(seamFound, seam);
		for (int i = 0; i < seam.length; i++) {
			total += sc.energy(seam[i], i);
		}
		assertEquals(total, 779648, 0.0001);

		pic = new Picture("test/7x3.png");
		sc = new SeamCarver(pic);
		seam = new int[] {2, 3, 2};
		seamFound = sc.findVerticalSeam();
		total = 0;
		for (int i = 0; i < seam.length; i++) {
			total += sc.energy(seam[i], i);
		}
		assertArrayEquals(seamFound, seam);
		assertEquals(total, 438090, 0.0001);

		pic = new Picture("test/6x5.png");
		sc = new SeamCarver(pic);
		seam = new int[] {2, 3, 3, 3, 2};
		seamFound = sc.findVerticalSeam();
		total = 0;
		for (int i = 0; i < seam.length; i++) {
			total += sc.energy(seam[i], i);
		}
		assertArrayEquals(seamFound, seam);
		assertEquals(total, 472025, 0.0001);

		pic = new Picture("test/10x12.png");
		sc = new SeamCarver(pic);
		seam = new int[] {5, 6, 7, 8, 7, 7, 6, 7, 6, 5, 6, 5};
		seamFound = sc.findVerticalSeam();
		total = 0;
		for (int i = 0; i < seam.length; i++) {
			total += sc.energy(seam[i], i);
		}
		assertArrayEquals(seamFound, seam);
		assertEquals(total, 652800, 0.0001);

	}

	public void TestFindHorizontalSeam() {

		StdOut.println("--- TEST FindHorizontalSeam ---");
		Picture pic = new Picture("test/3x7.png");
		SeamCarver sc = new SeamCarver(pic);
		int[] seam = new int[] {1, 2, 1};
		int[] seamFound = sc.findHorizontalSeam();
		double total = 0;
		assertArrayEquals(seamFound, seam);
		for (int i = 0; i < seam.length; i++) {
			total += sc.energy(i, seam[i]);
		}
		assertEquals(total, 445925, 0.0001);
		pic = new Picture("test/10x12.png");
		sc = new SeamCarver(pic);
		seam = new int[] {8, 9, 10, 10, 10, 9, 10, 10, 9, 8};
		seamFound = sc.findHorizontalSeam();
		total = 0;
		for (int i = 0; i < seam.length; i++) {
			total += sc.energy(i, seam[i]);
		}
		assertArrayEquals(seam, seamFound);
		assertEquals(total, 644531, 0.0001);
	}


	public static  void main(String[] args) {

		StdOut.printf("Start...\n\n");

		TestSeamCarver tester = new TestSeamCarver();

		try {
		    tester.TestCreation();
		    tester.TestEnergy();
		    tester.TestFindVertivalSeam();
		    tester.TestFindHorizontalSeam();
		}

		catch (java.lang.AssertionError e) {
		    StdOut.println(e.toString());
		}

		StdOut.printf("\n\n...finish.\n");
    }
}