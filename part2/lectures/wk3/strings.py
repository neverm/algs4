import timeit

test = """
s = "a" * 200000
for x in range(len(s)-5):
    some = (s[x:x+1] == s[x:] or s[x+1:x] == s[:x])

"""

time = timeit.timeit(test, number=1)

print time