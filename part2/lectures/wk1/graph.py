"""
Graph datastructure
"""

from sets import Set


class Graph(object):
    """Represent undirected unweighted graph"""
    def __init__(self):
        self.V = 0
        self.vs = []
        self.adj = {}

    def add_vertex(self, v):
        """add new vertex to the graph"""
        if v in self.vs:
            #raise ValueError("Vertex already exists")
            return
        self.vs.append(v)
        self.adj[v] = Set()
        self.V +=1

    def add_edge(self, v, w):
        """add edge between v and w"""
        if v not in self.vs or w not in self.vs:
            raise ValueError("Vertex doesn't exist")
        self.adj[v].add(w)
        self.adj[w].add(v)

    def get_adjacent(self, v):
        """get vertices adjacent to given vertex v"""
        if v not in self.vs:
            raise ValueError("Vertex doesn't exist")
        return self.adj[v]

    def get_vertices(self):
        """get all vertices"""
        return self.vs[:]

    def parse_file(self, file):
        """load digraph from file in following format:
        v w  - each line is space separated pair of vertices that form edge"""
        with open(file) as f:
            content = f.readlines()
        #lines = [line.strip() for line in content]
        for l in content:
            v, w = l.strip().split(" ")
            self.add_vertex(v)
            self.add_vertex(w)
            self.add_edge(v, w)

class Digraph(Graph):
    """Directed graph"""
    def add_edge(self, v, w):
        """add edge between v and w"""
        if v not in self.vs:
            raise ValueError("Vertex doesn't exist %s" % str(v))
        if w not in self.vs:
            raise ValueError("Vertex doesn't exist %s" % str(w))
        self.adj[v].add(w)

    def reverse(self):
        """returns copy of this graph with all edges reversed"""
        rev = self.__class__()
        for src, dsts in self.adj.items():
            rev.add_vertex(src)
            for dst in dsts:
                rev.add_vertex(dst)
                rev.add_edge(dst, src)
        return rev

class Edge(object):
    """Edge class for weighted graph"""
    def __init__(self, v, w, weight):
        self.v = v
        self.w = w
        self.weight = weight

    def get_either(self):
        """get any vertex"""
        return self.v

    def get_other(self, vertex):
        """get another vertex, different from provided"""
        if self.v == vertex:
            return self.w
        else:
            return self.v
        

class WeightedGraph(Graph):
    """Undirected graph with weights on edges"""

    """Represent undirected unweighted graph"""
    def __init__(self):
        self.V = 0
        self.vs = []
        self.adj = {}

    def add_vertex(self, v):
        """add new vertex to the graph"""
        if v in self.vs:
            #raise ValueError("Vertex already exists")
            return
        self.vs.append(v)
        self.adj[v] = Set()
        self.V +=1

    def add_edge(self, v, w):
        """add edge between v and w"""
        if v not in self.vs or w not in self.vs:
            raise ValueError("Vertex doesn't exist")
        self.adj[v].add(w)
        self.adj[w].add(v)

    def get_adjacent(self, v):
        """get vertices adjacent to given vertex v"""
        if v not in self.vs:
            raise ValueError("Vertex doesn't exist")
        return self.adj[v]

    def get_vertices(self):
        """get all vertices"""
        return self.vs[:]

    def parse_file(self, file):
        pass