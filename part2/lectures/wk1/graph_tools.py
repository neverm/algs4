from graph import *
from collections import deque

class DFS(object):
    """Depth First Search routine 
    for given graph and starting point"""
    def __init__(self, graph, s):
        self.graph = graph
        self.s = s
        self.visited = Set()
        self.cameFrom = {}
        self.__dfs(s, s)

    def __dfs(self, s, v):
        """visit vertex v from s recursively"""
        if v in self.visited:
            return
        self.visited.add(v)
        self.cameFrom[v] = s
        for w in self.graph.get_adjacent(v):
            self.__dfs(v, w)

    def get_path(self, v):
        """get path from source to given vertex"""
        if v not in self.visited:
            return []
        path = []
        while v != self.s:
            path.append(v)
            v = self.cameFrom[v]
        path.append(self.s)
        path.reverse()
        return path

class ExtDFS(DFS):
    """Extended Depth First Search, that supports adding
    preorder and postorder user functions"""
    def __init__(self, graph, s, preorder=None, postorder=None):
        self.preorder = preorder
        self.postorder = postorder
        self.graph = graph
        self.s = s
        self.visited = Set()
        self.cameFrom = {}
        self.__dfs(s, s)

    def __dfs(self, s, v):
        """visit vertex v from s recursively 
        and call preorder and postorder functions"""
        if v in self.visited:
            return
        self.visited.add(v)
        self.cameFrom[v] = s
        if self.preorder is not None:
            self.preorder(v)
        for w in self.graph.get_adjacent(v):
            self.__dfs(v, w)        
        if self.postorder is not None:
            self.postorder(v)
        

class BFS(object):
    """Bread first search routine for given starting point"""
    def __init__(self, graph, s):
        self.graph = graph
        self.s = s
        self.visited = Set()
        self.cameFrom = {}
        self.__bfs(s)

    def __bfs(self, v):
        d = deque([])
        d.append(v)
        currentDepth = 0
        self.cameFrom[v] = (v, currentDepth)
        while len(d) > 0:
            self.visited.add(v)
            dummy, currentDepth = self.cameFrom[v]
            for w in self.graph.get_adjacent(v):
                if w not in self.visited:
                    self.cameFrom[w] = (v, currentDepth+1)
                    d.append(w)
            v = d.popleft()

    def get_path(self, v):
        """get path from source to given vertex"""
        if v not in self.visited:
            return []
        path = []
        while v != self.s:
            path.append(v)
            v, length = self.cameFrom[v]
        path.append(self.s)
        path.reverse()
        return path



if __name__ == '__main__':
    g = Graph()

    for x in xrange(10):
        g.add_vertex(x)

    g.add_edge(1, 2)
    g.add_edge(1, 3)
    g.add_edge(3, 4)
    g.add_edge(4, 5)
    g.add_edge(5, 6)
    g.add_edge(6, 7)
    g.add_edge(7, 1)


    d = DFS(g, 1)
    print d.get_path(5)
    print d.get_path(1)
    print d.get_path(2)

    g1 = Graph()
    g1.parse_file("g1.txt")

    b=BFS(g, 1)
    path = b.get_path(4)
    print path, len(path)


    ps = []
    g2 = ExtDFS(g, 1, None, lambda x: ps.append(x))


    g3 = Digraph()
    g3.add_vertex(0)
    g3.add_vertex(1)
    g3.add_edge(0, 1)

    weighted1 = WeightedGraph()
    weighted1.add_vertex(0)
    weighted1.add_vertex(1)
    weighted1.add_edge(0, 1, 5)    

    print weighted1.adj