public class Quicksort {

	public static void sort(Comparable[] a) {

		//StdRandom.shuffle(a);
		int l = a.length;
		innerSort(a, 0, l-1);

	}

	private static void innerSort(Comparable[] a, int lo, int hi) {

		if (lo < hi) {
			int v = partition(a, lo, hi);
			innerSort(a, lo, v-1);
			innerSort(a, v+1, hi);
		}
	}

	private static int partition(Comparable[] a, int lo, int hi) {

		int i = lo;
		int j = hi + 1; 
		while (true) {

			//move i until hit element greater than lo
			while (less(a[++i], a[lo])) {
				if (i == hi) break;
			}
			//move j until hit element lesser than lo
			while (less(a[lo], a[--j])) {
				if (j == lo) break;
			}

			//check if i >= j (pointers crossed), then break
			if (i >= j) break;

			//exchange elements a[i] and a[j] to maintain invariant
			exchange(a, i, j);

		}

		exchange(a, lo, j);
		return j;
	}

	private static boolean less(Comparable x1, Comparable x2) {

		return x1.compareTo(x2) < 0;
	}

	private static void exchange(Comparable[] array, int i, int j) {

		Comparable tmp = array[i];
		array[i] = array[j];
		array[j] = tmp;
	}

	private static void print_r(Comparable[] array) {

		for (int i = 0; i < array.length; i++) {
			System.out.printf(" %d ", array[i]);
		}
		System.out.println();
	}

	public static void main(String[] args) {
		
		int N = 2000000;
		Integer[] arr = new Integer[N];
		for (Integer i = 0; i < N; i++) {
			arr[i] = StdRandom.uniform(0, 3000	);
		}

		sort(arr);
		
		for (Integer i = 0; i < 50; i++) {
			System.out.printf("%d ", arr[i]);
		}
		System.out.println();

		//Shellsort<String> s = new Shellsort<String>();
	}

}