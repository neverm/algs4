class Mergesorter(object):
    """Implements mergesort algorithm"""

    def __init__(self):
        super(Mergesorter, self).__init__()

    def sort(self, lst):
        
        self.lst = lst
        l = len(lst)
        if (l > 1):
            self.aux = [0] * l
            self._sort(0, l-1)
        return self.lst

    def _sort(self, lo, hi):

        if (hi - lo) > 0:
            mid = (hi + lo) / 2            
            self._sort(lo, mid)
            self._sort(mid+1, hi)
            self._merge(lo, hi, mid)

    def _merge(self, lo, hi, mid):

        i = lo
        j = mid+1
        k = lo
        while k <= hi:
            if i > mid:
                c = self.lst[j]
                self.aux[k] = c
                j +=1
            elif j > hi:
                self.aux[k] = self.lst[i]
                i += 1
            elif self.lst[i] < self.lst[j]:
                self.aux[k] = self.lst[i]
                i += 1
            else:
                self.aux[k] = self.lst[j]
                j += 1
            k += 1
        for x in xrange(lo, hi+1):
            self.lst[x] = self.aux[x]


if __name__ == '__main__':
    
    M = Mergesorter()

    print M.sort([5, 4, 5, 2, 5, 0])
