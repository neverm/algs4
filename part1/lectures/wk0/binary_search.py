import unittest

class BinarySearch(object):

	def __init__(self, lst):
		self.lst = lst

	"""
	return position of value in list using binary binary_search -1 if not found
	Assume: list is sorted
	"""
	def find(self, value):
		l = len(self.lst)
		return self.inner(value, 0, l-1)

	def inner(self, value, start, end):
		mid = (end+start) / 2
		test_element = self.lst[mid]
		if test_element == value:
			return mid
		if start >= end:
			return -1
		if value < test_element:
			return self.inner(value, start, mid-1)
		else:
			return self.inner(value, mid+1, end)



class TestBinarySearch(unittest.TestCase):

	def testFindsFirst(self):
		bs = BinarySearch([1, 2, 3])
		assert bs.find(1) == 0

	def testFindsLast(self):
		bs = BinarySearch([1, 2, 3, 4, 5])
		assert bs.find(5) == 4

	def testFindsMiddle(self):
		bs = BinarySearch([1, 2, 3, 5, 7, 10])
		assert bs.find(7) == 4

	def testNotFound(self):
		bs = BinarySearch([1, 2, 3])
		assert bs.find(5) == -1


if __name__ == '__main__':
	unittest.main()