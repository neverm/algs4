from nose import with_setup
from binary_search import BinarySearch

bs = None 

def setupFunction():
	print "Setup"
	global bs
	bs = BinarySearch([1, 2, 3, 5, 6, 11, 25])


def tearDownFunction():
	print "Tear down"

@with_setup(setupFunction)
def testFindsFirst():
	assert bs.find(1) == 0

@with_setup(setupFunction)
def testFindsLast():
	assert bs.find(25) == 6

@with_setup(setupFunction)
def testFindsSome():
	assert bs.find(3) == 2

@with_setup(setupFunction)
def testNotFound():
	assert bs.find(43) == -1