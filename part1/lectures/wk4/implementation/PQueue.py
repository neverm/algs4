import unittest

class MaxPQueue():

    def __init__(self):
        
        self._heap = [None]
        self._N = 0

    def getSize(self):
        
        return self._N

    def insert(self, value):

        self._heap.append(value)
        self._N += 1
        self._swim(self._N)

    def getMax(self):
        
        return self._heap[1]

    def removeMax(self):

        self._swap(1, self._N)
        maximum = self._heap.pop()
        self._N -= 1
        self._sink(1)
        return maximum

    def _sink(self, i):
        
        while 2*i <= self._N:
            j = 2*i
            #set j to be index of max child
            if j < self._N and self._cmp(j+1, j):
                j += 1
            #if element i is greater than max child - we are done
            if self._cmp(i, j):
                break
            #else swap it with max and continue on the swapped position
            self._swap(i, j)
            i = j


    def _swim(self, i):
        
        while i > 1 and self._cmp(i, i/2):
            self._swap(i, i/2)
            i = i/2
            

    def _swap(self, i, j):

        tmp = self._heap[i]
        self._heap[i] = self._heap[j]
        self._heap[j] = tmp

    def _cmp(self, i, j):
        return self._heap[i] > self._heap[j]

    def __str__(self):
        return str(self._heap[1:])

class MinPQueue(MaxPQueue):

    def _cmp(self, i, j):
        return self._heap[i] < self._heap[j]
        
        


class TestMaxPQueue(unittest.TestCase):

    def testInsert(self):
        pq = MaxPQueue()
        pq.insert(1)
        pq.insert(2)
        pq.insert(5)
        pq.insert(3)
        assert pq.getMax() == 5

    def testRemove(self):
        pq = MaxPQueue()
        pq.insert(1)
        pq.insert(2)
        pq.insert(5)
        pq.insert(3)
        assert pq.removeMax() == 5
        assert pq.removeMax() == 3


if __name__ == '__main__':
    unittest.main()