import itertools

class BST(object):
    """Binary search tree structure"""

    class Node(object):
        """Node of tree"""
        def __init__(self, key, value, is_red=False):
            self.key = key
            self.value = value
            self.count = 1
            self.left = None
            self.right = None
            self.color = is_red

        def clone(self):
            new = Node(self.key, self.value, self.color)
            new.count = self.count
            new.left = self.left
            new.right = self.right
            return new


        def __repr__(self):

            s = "\t%s (%s)\t" % (self.key, self.value)
            return s

    def __init__(self):
        self._root = None

    """Insert key-value pair to appropriate place in the tree"""
    def insert(self, key, value):
        self._root = self._insert(self._root, key, value)

    """Insert value associated with key to given node"""
    def _insert(self, node, key, value):
        if node is None:
            return self.Node(key, value, True)
        if key < node.key:
            node.left = self._insert(node.left, key, value)
        elif key > node.key:
            node.right = self._insert(node.right, key, value)
        else:
            node.value = value
        node.count = 1 + self._size(node.left) + self._size(node.right)
        if self._is_red(node.right) and not self._is_red(node.left):
            node = self._rotate_left(node)
        if self._is_red(node.left) and self._is_red(node.left.left):
            node = self._rotate_right(node)
        if self._is_red(node.left) and self._is_red(node.right):
            self._flip_color(node)
        return node

    def _is_red(self, node):
        if node is None:
            return False
        else:
            return node.color
    """
    rotates left given node
    usage: node = self._rotate_left(node)
    """
    def _rotate_left(self, node):
        assert(self._is_red(node.right))
        x = node.right
        node.right = x.left
        x.left = node
        x.color = node.color
        node.color = True
        return x

    """
    rotates right given node
    usage: node = self._rotate_right(node)
    """
    def _rotate_right(self, node):
        assert(self._is_red(node.left))
        x = node.left
        node.left = x.right
        x.right = node
        x.color = node.color
        node.color = True
        return x

    def _flip_color(self, node):
        if node is not None:
            node.color = not node.color
        if node.right is not None:
            node.right.color = not node.right.color
        if node.left is not None:
            node.left.color = not node.left.color

    def get(self, key):
        return  self._get(self._root, key)

    def _get(self, node, key):
        if node is None:
            return None
        if key < node.key:
            return self._get(node.left, key)
        elif key > node.key:
            return self._get(node.right, key)
        else:
            return node.value

    def _size(self, node):

        if node is not None:
            return node.count
        else:
            return 0

    def size(self):
        return self._size(self._root)


    def contains(self, key):
        return self.get(key) is not None

    def min(self):
        return self._min(self._root)

    def _min(self, node):
        if node.left is not None:
            return self._min(node.left)
        else:
            return node.value

    def max(self):
        return self._max(self._root)

    def _max(self, node):
        if node.right is not None:
            return self._max(node.right)
        else:
            return node.value

    def floor(self, key):

        node = self._floor(self._root, key)
        if node is not None:
            return node.value

    def _floor(self, node, key):
        if node is None:
            return None
        if key == node.key:
            return node
        if key < node.key:
            return self._floor(node.left, key)
        if key > node.key:
            min_right = self._floor(node.right, key)
            if min_right is not None:
                return min_right
            else:
                return node

    def traverse(self, node):

        if node.left is not None:
            for element in self.traverse(node.left):
                yield element
        yield node.value
        if node.right is not None:
            for element in self.traverse(node.right):
                yield element

    def select(self, n):
        try:
            nth = next(itertools.islice(self.__iter__(), n, n+1))
            return nth
        except StopIteration:
            return None

    def __repr__(self):
        return self._root.__repr__()

    def __iter__(self):
        return self.traverse(self._root)


if __name__ == '__main__':
    bst = BST()
    bst.insert("a", 5)
    bst.insert("d", 2)
    bst.insert("r", 15)
    bst.insert("e", 11)
    bst.insert("h", 5)
    bst.insert("w", 1)
    bst.insert("z", 100)
    #print bst.get("a")
    print [node for node in bst]
    print bst