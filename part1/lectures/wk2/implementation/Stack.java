public class Stack {

	private Node first;
	private int count;

	private class Node {

		String item;
		Node next;
	}

	public Stack() {

		Node first = null;
	}

	public String pop() {

		if (first != null) {
			String item = first.item;
			Node next = first.next;
			first = next;
			return item;
		}
		else 
			return null;
	}

	public void push(String s) {

		Node newfirst = new Node();
		newfirst.item = s;
		newfirst.next = first;
		first = newfirst;
	}

	public static void main(String[] args) {

		Stack st = new Stack();
		st.push("a");
		System.out.println(st.pop());
	}
}