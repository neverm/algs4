import java.util.Iterator;

public class QueueArray<Item> implements Iterable<Item> {


	private Item[] q;
	private int head, tail, capacity, count;
	
	public QueueArray() {

		capacity = 1;
		count = 0;
		head = 0;
		tail = 0;
		q = (Item[]) new Object[1];
	}

	public Iterator<Item> iterator() {return new QueueIterator(); }

	private class QueueIterator implements Iterator<Item> {

		private int position = head;
		public boolean hasNext() { return position < tail;}
		public Item next() { return q[position++]; }
		public void remove() {throw new UnsupportedOperationException("Removing is not supported");}
	}

	public void enqueue(Item item) {

		if (tail == capacity) {
			resize(capacity*2);
		}
		q[tail] = item;
		tail++;
		count++;
	}

	public Item dequeue() {

		Item item = q[head];
		q[head] = null;
		if (count <= capacity/4) {
			resize(capacity/2);
		}
		head++;
		count--;
		return item;
	}

	private void resize(int size) {

		Item[] newqueue = (Item[]) new Object[size];
		for (int i = 0; i < count; i++) {
			newqueue[i] = q[head+i];
		}
		q = newqueue;
		capacity = size;
	}

	private void printq() {

		System.out.print("|");
		for (int i = 0; i < capacity; i++) {
			System.out.printf("%s|", q[i]);
		}
		System.out.println();
	}

	public static void main(String[] args) {

		QueueArray<String> q = new QueueArray<String>();
		q.enqueue("s");
		q.enqueue("u");
		q.enqueue("d");
		q.enqueue("a");
		q.enqueue(" ");
		q.enqueue("i");
		q.enqueue("d");
		q.enqueue("i");
		q.enqueue("!");

		q.dequeue();
		q.dequeue();

		//q.printq();

		for (String s: q) {
			System.out.printf("|| %s || ", s);
		}

		/*
		for (int i = 0; i < 25000; i++) {
			q.enqueue(String.format("alalala %d", i));
		}
		*/
	}
}