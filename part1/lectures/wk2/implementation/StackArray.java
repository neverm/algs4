public class StackArray {


	private String[] st;
	private int capacity;
	private int N;
	
	public StackArray() {
		capacity = 1;
		N = 0;
		st = new String[1];
	}

	public void push(String item) {

		if (N == capacity) {
			resize(capacity*2);
		}
		st[N++] = item;
	}

	public String pop() {

		String item = st[--N];
		st[N] = null;
		if (N < capacity/4) {
			resize(capacity/2);
		}
		return item;
	}

	private void resize(int size) {

		String[] newstack = new String[size];
		for (int i = 0; i < N; i++) {
			newstack[i] = st[i];
		}
		st = newstack;
		capacity = size;
	}

	public static void main(String[] args) {

		StackArray st = new StackArray();
		
		for (int i = 0; i < 25000; i++) {
			st.push(String.format("alalala %d", i));
		}
	}
}