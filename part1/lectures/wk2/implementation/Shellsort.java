public class Shellsort {

	public static void sort(Comparable[] a) {

		int N = a.length;

		int h = 1;
		while (h < N/3) { h = 3*h + 1; }

		
		while (h >= 1) {
			
			for (int i = h; i < N; i++) {
				for (int j = i; j >= h; j-=h) {
					if (less(a[j], a[j-h])) {
						exchange(a, j, j-h);
					}
				}
			}
		h = h/3;
		}
	}

	private static boolean less(Comparable x1, Comparable x2) {

		return x1.compareTo(x2) < 0;
	}

	private static void exchange(Comparable[] array, int i, int j) {

		Comparable tmp = array[i];
		array[i] = array[j];
		array[j] = tmp;
	}

	public static void main(String[] args) {
		
		int N = 20000;
		Integer[] arr = new Integer[N];
		for (Integer i = 0; i < N; i++) {
			arr[i] = StdRandom.uniform(0, 2000);
		}

		sort(arr);
		
		for (Integer i = 0; i < 50; i++) {
			System.out.printf("%d ", arr[i]);
		}
		System.out.println();
		

		//Shellsort<String> s = new Shellsort<String>();
	}

}