import unittest
import random

class QuickFind(object):

	def __init__(self, n):
		self.n = n
		self.components = [x+1 for x in xrange(n)]

	def checkArgs(self, p, q=0):
		
		if p > self.n and q > self.n:
			raise ValueError("no such element")

	def connect(self, p, q):
		
		self.checkArgs(p, q)
		for c in xrange(self.n):
			if self.components[c] == p:
				self.components[c] = q

	def find(self, p, q):

		self.checkArgs(p, q)
		return self.components[p-1] == self.components[q-1]


class QuickUnion(QuickFind):

	def __init__(self, n):
		self.n = n
		self.parents = [x+1 for x in xrange(n)]
		self.weights = [1] * n

	def get_root(self, p):

		self.checkArgs(p)
		root = self.parents[p-1]
		path = []
		while root != self.parents[root-1]:
			path.append(root)
			root = self.parents[root-1]
			#self.parents[root-1] = self.parents[self.parents[root-1]]
		for node in path:
			self.parents[node-1] = root
		return root


	def connect(self, p, q):

		self.checkArgs(p, q)
		q_root = self.get_root(q)
		p_root = self.get_root(p)
		if self.weights[q_root-1] > self.weights[p_root-1]:
			self.parents[p_root-1] = q_root
			self.weights[q_root-1] += self.weights[p_root-1]
		else:
			self.parents[q_root-1] = p_root
			self.weights[p_root-1] += self.weights[q_root-1]

	def find(self, p, q):
		
		self.checkArgs(p, q)
		return self.get_root(p) == self.get_root(q)



class TestConnectivity(unittest.TestCase):

	def testQuickFindSuccess(self):
		qf = QuickFind(10)
		qf.connect(1, 10)
		assert qf.find(1,10) == True

	def testQuickFindFailure(self):
		qf = QuickFind(10)
		qf.connect(1, 5)
		assert qf.find(1,10) == False


	def testQuickUnionSuccess(self):
		qu = QuickUnion(10)
		qu.connect(1, 10)
		assert qu.find(1,10) == True

	def testQuickUnionFailure(self):
		qu = QuickUnion(10)
		qu.connect(1, 5)
		assert qu.find(1,10) == False

	def testQuickUnionSpeed(self):
		huge = 110000
		qu = QuickUnion(huge)
		for x in xrange(huge-1):
			qu.connect(random.randrange(huge),random.randrange(huge))
		for x in xrange(huge):
			a = qu.find(x, random.randrange(huge))
		assert True

if __name__ == '__main__':
	unittest.main()
	raw_input()