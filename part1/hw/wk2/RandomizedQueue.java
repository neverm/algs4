import java.util.Iterator;

public class RandomizedQueue<Item> implements Iterable<Item> {


	private int N, count, capacity;
	private Item[] arr;

	public RandomizedQueue() {

		capacity = 1;
		arr = (Item[]) new Object[capacity];
		count = 0;
		N = 0;
	}

	public boolean isEmpty() {

		return N == 0;
	}

	public int size() {

		return N;
	}

	public void enqueue(Item item) {

		if (item == null) {
			throw new NullPointerException();
		}
		if (N == capacity) {
			resize(capacity*2);
		}
		arr[N++] = item;

	}

	public Item dequeue() {

		if (N == 0) {
			throw new java.util.NoSuchElementException("Queue is empty");
		}
		int k = StdRandom.uniform(0, N);
		swap(arr, k, N-1);
		Item item = arr[--N];
		arr[N] = null;
		if (N < capacity / 4) {
			resize(capacity/2);
		}
		return item;
	}

	public Item sample() {
		
		if (N == 0) {
			throw new java.util.NoSuchElementException("Queue is empty");
		}
		int k = StdRandom.uniform(0, N);
		return arr[k];
	}

	private void resize(int size) {

		Item[] resized = (Item[]) new Object[size];
		for (int i = 0; i < N; i++) {
			resized[i] = arr[i];
		}
		arr = resized;
		capacity = size;
	}

	private void swap(Item[] arr, int v, int w) {

		Item tmp = arr[v];
		arr[v] = arr[w];
		arr[w] = tmp;
	}

	public Iterator<Item> iterator() { return new RandomizedQueueIterator(); }

	private class RandomizedQueueIterator implements Iterator<Item> {

		private Item[] randomArr;
		private int current;

		public RandomizedQueueIterator() {

			current = 0;
			randomArr = (Item[]) new Object[N];
			for (int i = 0; i < N; i++) {
				randomArr[i] = arr[i];
				int k = StdRandom.uniform(0, i+1);
				swap(randomArr, i, k);
			}
		}

		public boolean hasNext() { return current < N; }

		public Item next() { 
			if (current >= N) {
				throw new java.util.NoSuchElementException("end");
			}
			return randomArr[current++];
		}

		public void remove() { throw new UnsupportedOperationException("Removing is not supported"); }
	}


	public static void main(String[] args) {

		RandomizedQueue<Integer> rq = new RandomizedQueue<Integer>();
		
		for (int i = 0; i < 50; i++) {
			rq.enqueue(i);
		}
		
		int sum = 0;
		for (int item: rq) {
			System.out.printf(" %d ", item);
		}
	}

}