import java.util.Iterator;

public class Deque<Item> implements Iterable<Item> {

	private Node first, last;
	private int count;

	public Deque() {

		count = 0;
		first = null;
		last = null;
	}

	private class Node {

		Item item;
		Node next;
		Node prev;
	}

	public Iterator<Item> iterator() { return new DequeIterator();}

	private class DequeIterator implements Iterator<Item> {

		private Node current = first;

		public boolean hasNext() { return current != null;}
		public Item next() {
			if (current == null) {
				throw new java.util.NoSuchElementException("Deque is is empty");
			}
			Item item = current.item;
			current = current.next;
			return item;
		}
		public void remove() {throw new UnsupportedOperationException("Removing is not supported");}
	}

	public boolean isEmpty() {

		return count == 0;
	}

	public int size() {

		return count;
	}

	public void addFirst(Item item) {

		if (item == null) {
			throw new NullPointerException("Null is not allowed");
		}
		Node newfirst = new Node();
		newfirst.item = item;
		newfirst.next = first;
		if (first != null) {
			first.prev = newfirst;
		}
		first = newfirst;
		if (last == null) {
			last = first;
		}
		count++;
	}

	public void addLast(Item item) {

		if (item == null) {
			throw new NullPointerException("Null is not allowed");
		}
		Node newlast = new Node();
		newlast.item = item;
		newlast.prev = last;
		if (last != null) {
			last.next = newlast;
		}
		last = newlast;
		if (first == null) {
			first = last;
		}
		count++;
	}

	public Item removeFirst() {

		if (isEmpty()) {
			throw new java.util.NoSuchElementException("Deque is is empty");
		}
		Item item = first.item;
		Node second = first.next;
		first = second;
		if (first != null) {
			first.prev = null;
		}
		else {
			last = null;
		}
		
		count--;
		return item;
	}

	public Item removeLast() {

		if (isEmpty()) {
			throw new java.util.NoSuchElementException("Deque is is empty");
		}
		Item item = last.item;
		Node prelast = last.prev;
		last = prelast;
		if (last != null) {
			last.next = null;
		}
		else {
			first = null;
		}
		count--;
		return item;
	}

	

	public static void main(String[] args) {

		Deque<String> d = new Deque<String>();
		d.addFirst("first1");
		d.addFirst("first2");
		d.removeFirst();
		d.addLast("last1");
		d.addLast("last2");
		d.removeFirst();
		d.addFirst("aka");
		d.removeFirst();
		for (String s: d) {
			System.out.println(s);
		}
		
		/* /
		System.out.println(d.removeLast());
		System.out.println(d.removeFirst());
		System.out.println(d.removeLast());
		System.out.println(d.removeFirst());
		/**/
	}
}