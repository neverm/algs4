public class Solver {


    // find a solution to the initial board (using the A* algorithm)
    private Stack<Board> solutionTrace;
    private int moves;
    public Solver(Board initial)  {

    	moves = 0;
        solve(initial);
    }

    private void solve(Board b) {

        MinPQ<Node> pq = new MinPQ<Node>();
        MinPQ<Node> pq_twin = new MinPQ<Node>();
        solutionTrace = new Stack<Board>();
        Node current = new Node(b, 0, null);
        Node current_twin = new Node(b.twin(), 0, null);
        pq.insert(current);
        pq_twin.insert(current_twin);

        
        while (!current.isSolution()) {
        	current = pq.delMin();
        	current_twin = pq_twin.delMin();
        	for (Node next : current.getNextNodes()) {
        		pq.insert(next);
        	}
        	for (Node next : current_twin.getNextNodes()) {
        		pq_twin.insert(next);
        	}
        	if (current_twin.isSolution()) {
	    		solutionTrace = null;
	    		moves = -1;
	    		break;
	    	}
        }
        if (current.isSolution()) {
    		moves = current.getMoves();
    		do {
    			solutionTrace.push(current.getBoard());	
    			current = current.getPrev();
    		} 
    		while (current != null);
    	}
    }

    private class Node implements Comparable<Node> {

        private Board board;
        private Node prev;
        private int moves, priority;

        public Node(Board b, int m, Node p) {
            
            board = b;
            prev = p;
            moves = m;
        }

        public int getPriority() {

            return board.manhattan() + moves;
        }

        public int compareTo(Node that) {

        	if (this.getPriority() < that.getPriority()) return -1;
        	if (this.getPriority() > that.getPriority()) return 1;
        	return 0;
        }

        public Iterable<Node> getNextNodes() {

        	Bag<Node> nexts = new Bag<Node>();
        	for (Board b : board.neighbors()) {
        		if (prev == null || !b.equals(prev.getBoard())) {
        			nexts.add(new Node(b, moves+1, this));
        		}
        	}
        	return nexts;
        }

        public boolean isSolution() {

        	return board.isGoal();
        }

        public Board getBoard() {
        	return board;
        }

        public int getMoves() {return moves;}

        public Node getPrev() {
        	return prev;
        }

    }

    // is the initial board solvable?
    public boolean isSolvable() {

        return (moves != -1);
    }

    // min number of moves to solve initial board; -1 if no solution
    public int moves() {
        return moves;
    }

    // sequence of boards in a shortest solution; null if no solution
    public Iterable<Board> solution() {
        return solutionTrace;
    }

    // solve a slider puzzle (given below)
    public static void main(String[] args) {
        // create initial board from file
        In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);

        // solve the puzzle
        //Stopwatch solveTimer = new Stopwatch();
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
        //System.out.printf("\n Solving took %.5fs \n", solveTimer.elapsedTime());
    }
}