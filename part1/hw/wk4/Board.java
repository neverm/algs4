import java.lang.Math;

public class Board {

    private int[][] board;
    private int N, manhattan, hamming;
    // construct a board from an N-by-N array of blocks{
    // (where blocks[i][j] = block in row i, column j)
    public Board(int[][] blocks) {

        N = blocks.length;
        manhattan = -1;
        hamming = -1;
        board = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                board[i][j] = blocks[i][j];

    } 

    private Board(int[][] blocks, int manh) {

        this(blocks);
        manhattan = manh;
    }
     
    // board dimension N                                      
    public int dimension() {

        return N;
    }

    // number of blocks out of place   
    public int hamming() {

        int errors = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (board[i][j] != posToValue(i, j) && board[i][j] != 0) 
                    errors++;
            }
        }    
        return errors;
    }

    // sum of Manhattan distances between blocks and goal   
    public int manhattan() {

        if (manhattan == -1) {
            int distance = 0;
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    if (board[i][j] != 0) {
                        int val = board[i][j];
                        int original_i = (val-1) / N;
                        int original_j = (val-1) % N;
                        distance += Math.abs(i - original_i) + Math.abs(j - original_j);
                    }
                        
                }
            }
            manhattan = distance;
        }
        return manhattan;
    }

    // is this board the goal board?   
    public boolean isGoal() {

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (i == N-1 && j == N-1)
                    return board[i][j] == 0;
                if (board[i][j] != posToValue(i, j)) 
                    return false;
            }
        }
        return true;
    }

    // a board obtained by exchanging two adjacent blocks in the same row   
    public Board twin() {

        int[][] twinBoard = new int[N][N];
        for (int i = 0; i < N; i++) 
            for (int j = 0; j < N; j++)
                twinBoard[i][j] = board[i][j];
        if (twinBoard[0][0] != 0 && twinBoard[0][1] != 0)
            swap(twinBoard, 0, 0, 0, 1);
        else
            swap(twinBoard, 1, 0, 1, 1);
        return new Board(twinBoard);
    }

    // does this board equal y?   
    public boolean equals(Object y) {

        if (y == this) return true;
        if (y == null) return false;
        if (y.getClass() != this.getClass()) return false;
        Board that = (Board) y;  
        if (N != that.dimension()) return false;
        for (int i = 0; i < N; i++) 
            for (int j = 0; j < N; j++)
                if (this.board[i][j] != that.board[i][j])
                    return false;
        return true;
    }

    // all neighboring boards   
    public Iterable<Board> neighbors() {

        Bag<Board> neighbors = new Bag<Board>();
        int i = -1, j = -1;
        int min = 0, max = N-1;
        for (int y = 0; y < N; y++) {
            for (int x = 0; x < N; x++) {
                if (board[y][x] == 0) {
                    i = y;
                    j = x;
                }
            }
        }
        // !!! make new method for producing neighbor
        // in that method call private constructor, 
        // compute changed manhattan value for new position instead of recomputing manhattan
        if (j > min) {
            swap(board, i, j, i, j-1);
            neighbors.add(new Board(board));
            swap(board, i, j, i, j-1);
        }
        if (j < max) {
            swap(board, i, j, i, j+1);
            neighbors.add(new Board(board));
            swap(board, i, j, i, j+1);
        }
        if (i > min) {
            swap(board, i, j, i-1, j);
            neighbors.add(new Board(board));
            swap(board, i, j, i-1, j);
        }
        if (i < max) {
            swap(board, i, j, i+1, j);
            neighbors.add(new Board(board));
            swap(board, i, j, i+1, j);
        }
        return neighbors;
    }

    // string representation of the board  
    public String toString() {

        StringBuilder s = new StringBuilder();
        s.append(N + "\n");
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                s.append(String.format("%2d ", board[i][j]));
            }
            s.append("\n");
        }
        return s.toString();
    }   

    private void swap(int[][] a, int i1, int j1, int i2, int j2) {
        
        if (i1 >= N || j1 >= N || i2 >= N || j2 >= N ||
            i1 < 0 || j1 < 0 || i2 < 0 || j2 < 0) {
            throw new java.lang.IllegalArgumentException(String.format("Wrong index: i1: %d, j1: %d, i2: %d, j2: %d", i1, j1, i2, j2));
        }
        int tmp = a[i1][j1];
        a[i1][j1] = a[i2][j2];
        a[i2][j2] = tmp;
    }

    //map position at i, j to value that should be there in goal board
    private int posToValue(int i, int j) {
        return (i*N)+j+1;
    }

    public static void main(String[] args) {

        In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);
        System.out.println(initial);
        for (Board b: initial.neighbors()) {
            System.out.println(b);
        }
    }
}