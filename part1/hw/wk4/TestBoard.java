import static org.junit.Assert.*;

public class TestBoard {

	public void TestGoal() {

		//double delta = 0.0001;
		StdOut.println("--- TEST isGoal ---");
		int N = 3;
		int[][] blocks = new int[N][N];
		for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = (i*N)+j+1;
		Board b1 = new Board(blocks);
		blocks[N-1][N-1] = 0;
		Board b2 = new Board(blocks);
		assertFalse(b1.isGoal());
		assertTrue(b2.isGoal());
	}

	public void TestTwin() {

		StdOut.println("--- TEST twin ---");
		int N = 3;
		int[][] blocks = new int[N][N];
		for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = (i*N)+j+1;		
		blocks[N-1][N-1] = 0;
		Board b1 = new Board(blocks);
		int tmp = blocks[0][0];
		blocks[0][0] = blocks[0][1];
		blocks[0][1] = tmp;
		Board twin = new Board(blocks);
		assertEquals(twin, b1.twin());

	}

	public void TestHamming() {

		StdOut.println("--- TEST hamming ---");
		int N = 3;
		int[][] blocks = new int[N][N];
		for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = (i*N)+j+1;
        blocks[N-1][N-1] = 0;
        Board b1 = new Board(blocks);
        int tmp = blocks[0][0];
		blocks[0][0] = blocks[0][1];
		blocks[0][1] = tmp;
		Board b2 = new Board(blocks);
		assertEquals(0, b1.hamming());
		assertEquals(b2.hamming(), 2);
	}

	public void TestManhattan() {

		StdOut.println("--- TEST manhattan ---");
		int N = 4;
		int[][] blocks = new int[N][N];
		for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = (i*N)+j+1;
        blocks[N-1][N-1] = 0;
        Board b1 = new Board(blocks);
        blocks[3][2] = 4;
        Board b2 = new Board(blocks);
        blocks[0][3] = 15;
        Board b3 = new Board(blocks);
		assertEquals(0, b1.manhattan());
		assertEquals(4, b2.manhattan());
		assertEquals(8, b3.manhattan());
	}


	public static  void main(String[] args) {

		StdOut.printf("Start...\n\n");

        TestBoard tester = new TestBoard();

        try {
        	tester.TestGoal();
        	tester.TestTwin();
        	tester.TestHamming();
        	tester.TestManhattan();
        }

        catch (Exception e) {
        	StdOut.println(e.toString());
        }

        StdOut.printf("\n\n...finish.\n");
	}

}