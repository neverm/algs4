public class KdTree {

	private int size;
	private Node root;
	private static class Node {

		private Point2D p; // point
		private RectHV rect; //rectangle that covers node
		private Node lb; //left/bottom subtree
		private Node rt; //right/top 

		public Node(Point2D point, RectHV r) {

			p = point;
			if (r == null) 
				r = new RectHV(0, 0, 1, 1);
			else {

			}
			rect = r;

		}
	}

	public KdTree() {

		size = 0;

	}

	public int size() {

		return size;
	}

	// is the set empty?
    public boolean isEmpty() {
        return (root == null);
    }

	public void draw() {

		draw(root, true);
	}

	private void draw(Node node, boolean byX) {

		if (node == null) return;
		StdDraw.setPenRadius(.01);
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.point(node.p.x(), node.p.y());
		StdDraw.setPenRadius();
		if (byX) {
			StdDraw.setPenColor(StdDraw.RED);
			StdDraw.line(node.p.x(), node.rect.ymin(), node.p.x(), node.rect.ymax());
		}
		else {
			StdDraw.setPenColor(StdDraw.BLUE);
			StdDraw.line(node.rect.xmin(), node.p.y(), node.rect.xmax(), node.p.y());
		}
		draw(node.lb, !byX);
		draw(node.rt, !byX);

	}

	public void insert(Point2D p) {
		
		root = insert(p, root, true, null);
	}

	private Node insert(Point2D p, Node node, boolean byX, RectHV nextRect) {

		double cmp;
		if (node == null) {
			size++;
			return new Node(p, nextRect);	
		} 
		if (p.equals(node.p)) return node;
		if (byX)
			cmp = p.x() - node.p.x();
		else
			cmp = p.y() - node.p.y();
		if (cmp < 0) {
			if (node.lb == null) {
				if (byX) nextRect = new RectHV(node.rect.xmin(), node.rect.ymin(), node.p.x(), node.rect.ymax());
				else nextRect = new RectHV(node.rect.xmin(), node.rect.ymin(), node.rect.xmax(), node.p.y());
			}
			node.lb = insert(p, node.lb, !byX, nextRect);
		}
		else if (cmp >= 0) {
			if (node.rt == null) {
				if (byX) nextRect = new RectHV(node.p.x(), node.rect.ymin(), node.rect.xmax(), node.rect.ymax());
				else nextRect = new RectHV(node.rect.xmin(), node.p.y(), node.rect.xmax(), node.rect.ymax());
			}
			node.rt = insert(p, node.rt, !byX, nextRect);	
		} 
		return node;
	}

	public boolean contains(Point2D p) {
		
		return contains(p, root, true);
	}

	private boolean contains(Point2D p, Node node, boolean byX) {

		double cmp;
		if (node == null) return false;
		if (p.equals(node.p)) return true;
		if (byX)
			cmp = p.x() - node.p.x();
		else
			cmp = p.y() - node.p.y();
		if (cmp < 0) return contains(p, node.lb, !byX);
		else return contains(p, node.rt, !byX); // if (cmp >= 0)
	}

	public Iterable<Point2D> range(RectHV rect) {

		Queue<Point2D> q = new Queue<Point2D>();
		checkRangeNode(root, rect, true, q);
		return q;
	}

	private void checkRangeNode(Node node, RectHV rect, boolean byX, Queue<Point2D> q) {

		if (node == null) return;

		if (rect.contains(node.p)) q.enqueue(node.p);
		if (rect.intersects(node.rect)) {
			checkRangeNode(node.rt, rect, !byX, q);
			checkRangeNode(node.lb, rect, !byX, q);
		}

	}

	private Iterable<Point2D> getCollection() {

		Queue<Point2D> q = new Queue<Point2D>();
		addToCollection(q, root);
		return q;
	}

	private void addToCollection(Queue<Point2D> q, Node node) {

		if (node == null) return;
		addToCollection(q, node.lb);
		q.enqueue(node.p);
		addToCollection(q, node.rt);
	}

	public Point2D nearest(Point2D p) {
		
		Point2D best = new Point2D(9999999, 9999999);
		best = nearest(p, best, root, true);
		if (best.x() == 9999999 || best.y() == 9999999)
			return null;
		return best;
	}

	private Point2D nearest(Point2D p, Point2D best, Node node, boolean byX) {

		if (node == null) return best;
		if (node.rect.distanceSquaredTo(p) > p.distanceSquaredTo(best)) return best;
		if (node.p.equals(p)) return node.p;
		double cmp;
		if (byX) 
			cmp = p.x() - node.p.x();
		else
			cmp = p.y() - node.p.y();
		if (node.p.distanceSquaredTo(p) < best.distanceSquaredTo(p))
			best = node.p;
		if (cmp > 0) {
			best = nearest(p, best, node.rt, !byX);
			best = nearest(p, best, node.lb, !byX);
		}
		else {
			best = nearest(p, best, node.lb, !byX);
			best = nearest(p, best, node.rt, !byX);	
		}
		return best;
	}

	public static void main(String[] args) {
		
		KdTree t = new KdTree();
		t.insert(new Point2D(0.9, 0.5));
		t.insert(new Point2D(0.2, 0.5));
		t.insert(new Point2D(0.3, 0.5));
		t.insert(new Point2D(0.4, 0.5));
		t.insert(new Point2D(0.1, 0.5));
		t.insert(new Point2D(0.6, 0.5));
		t.insert(new Point2D(0.5, 0.5));
		t.insert(new Point2D(0.7, 0.5));

		for (Point2D p: t.getCollection()) {
			System.out.println(p);
			System.out.println(t.contains(p));
		}
		System.out.println(t.nearest(new Point2D(1, 1)));
	}

}