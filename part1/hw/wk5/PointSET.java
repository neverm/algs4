import java.util.TreeSet;

public class PointSET {
    
    private TreeSet<Point2D> set;
    // construct an empty set of points
    public PointSET() {

        set = new TreeSet<Point2D>();
    }

    // is the set empty?
    public boolean isEmpty() {
        return set.isEmpty();
    }

    // number of points in the set
    public int size() {
        return set.size();
    }

    // add the point p to the set (if it is not already in the set)
    public void insert(Point2D p) {
        set.add(p);
    }

    // does the set contain the point p?
    public boolean contains(Point2D p) {
        return set.contains(p);
    }

    // draw all of the points to standard draw
    public void draw() {

        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(.01);
        for (Point2D p : set) {
            p.draw();
        }
    }

    // all points in the set that are inside the rectangle
    public Iterable<Point2D> range(RectHV rect) {

        Queue<Point2D> inside = new Queue<Point2D>();
        for (Point2D p : set) {
            if (rect.contains(p))
                inside.enqueue(p);
        }
        return inside;
    }

    // a nearest neighbor in the set to p; null if set is empty
    public Point2D nearest(Point2D p) {

        Point2D best = null;
        double bestDistance = Double.POSITIVE_INFINITY;
        for (Point2D point : set) {
            if (best == null) {
                best = point;
                bestDistance = p.distanceSquaredTo(point);
            }
            else {
                if (p.distanceSquaredTo(point) < bestDistance) {
                    best = point;
                    bestDistance = p.distanceSquaredTo(point);
                }
            }
        }
        return best;
    }

    public static void main(String[] args) {

        StdDraw.setXscale(0, 100);
        StdDraw.setYscale(0, 100);

        PointSET ps = new PointSET();
        ps.insert(new Point2D(50, 50));
        ps.insert(new Point2D(75, 50));
        ps.insert(new Point2D(50, 75));
        ps.insert(new Point2D(11, 25));
        ps.draw();

        for (Point2D p: ps.range(new RectHV(0, 0, 25, 25))) {
            System.out.println(p);
        }

    }

}