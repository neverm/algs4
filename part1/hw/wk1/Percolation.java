public class Percolation {
  
  
  private boolean[][] grid;
  private int N;
  private int totalSitesNumber;
  private int virtualTopIndex;
  private int virtualBottomIndex;
  private WeightedQuickUnionUF qu;
  
  public Percolation(int N) {
    
    this.N = N;
    this.grid = new boolean[N][N];
    // N x N grid + 2 virtual + 1 to avoid mapping 1 (percolation) to 0 (array)
    // values in quick union array will begin from 1 index, 0 will always be empty
    this.totalSitesNumber = N * N + 3;
    this.qu = new WeightedQuickUnionUF(totalSitesNumber);
    this.virtualBottomIndex = totalSitesNumber-1;
    this.virtualTopIndex = virtualBottomIndex-1;
    this.initVirtuals();
    for (int i = 0; i < N; i++) {
      for (int j = 0; j < N; j++) {
        grid[i][j] = false;
      }
    }
  }

  private void checkIndices(int i, int j) {

    if (i <= 0 || i > N || j <= 0 || j > N)
      throw new java.lang.IndexOutOfBoundsException("Index should be 0 < i <= N");
  }

  /**
  * Initialize virtual sites: connect each to their row and to each other
  */
  private void initVirtuals() {

    for (int i = 1; i <= N; i++) {
      qu.union(virtualTopIndex, mapToFlat(1, i));
      qu.union(virtualBottomIndex, mapToFlat(N, i));
    }
  }

  /**
  * Map given i, j coordinates on percolation 1-dimensional space
  * @param int i coordinate
  * @param int j coordinate
  * @return int 
  */

  private int mapToFlat(int i, int j) {

    return N*(i-1)+j;
  }

  /**
  * Get value located at i, j (1-based) on grid
  * Basically just map 1-based coordinates into 0-based array
  * @param int i coordinate
  * @param int j coordinate
  */
  private boolean getGrid(int i, int j) {

    return grid[i-1][j-1];
 }

  /**
  * set value located at i, j (1-based) on grid
  * Basically just map 1-based coordinates into 0-based array
  * @param int i coordinate
  * @param int j coordinate
  * @param boolean value to set
  */
  private void setGrid(int i, int j, boolean value) {

    grid[i-1][j-1] = value;
 }


  /**
  * Connect two sites (i1, j1) and (i2, j2)
  * @param int i1 coordinate of first site
  * @param int j1 coordinate of first site
  * @param int i2 coordinate of second site
  * @param int j2 coordinate of second site
  */
  private void connect(int i1, int j1, int i2, int j2) {
    if (isOpen(i1, j1) && isOpen(i2, j2)) {
      qu.union(mapToFlat(i1, j1), mapToFlat(i2, j2));
    }
  }

  /**
  * Connect (all possible from top, right, bottom and left) neighbors to given site
  * @param int i coordinate
  * @param int j coordinate
  */
  private void connectNeighbors(int i, int j) {

    //not in the top row
    if (i > 1) {
      connect(i, j, i-1, j);
    }
    //not in the bottom row
    if (i < N) {
      connect(i, j, i+1, j);
    }
    //not in the leftmost column
    if (j > 1) {
      connect(i, j, i, j-1);
    }
    //not in the rightmost column
    if (j < N) {
      connect(i, j, i, j+1);
    }
  }

  /**
  * Open a site and connect to all opened neighbors
  * @param int i coordinate
  * @param int j coordinate
  */
  public void open(int i, int j) {
    
    checkIndices(i, j);
    setGrid(i, j, true);
    connectNeighbors(i, j);
  }
  
  public boolean isOpen(int i, int j) {

    checkIndices(i, j);
    return getGrid(i, j);
  }
  
  public boolean isFull(int i, int j) {

    checkIndices(i, j);
    return (isOpen(i, j) && qu.connected(mapToFlat(i, j), virtualTopIndex));
  }
  
  public boolean percolates() {

    return qu.connected(virtualTopIndex, virtualBottomIndex);
    /*
    for (int i = 1; i <= N; i++) {
      if (isFull(N, i)) {
        return true;
      }
    }
    return false;
    */
  }

  private void eliminateBackwashes() {


  }
  
  
  public static void main(String[] args) {
    
    Percolation p = new Percolation(3);
    p.open(2, 2);
    p.open(1, 2);
    p.open(3, 2);
    if (p.percolates()) {
      System.out.println("suda idi");
      //System.out.printf("%d \n", p.mapToFlat(3, 1));
    }
    
  }
  
}