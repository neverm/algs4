import java.math.*;

public class PercolationStats {

	private double[] experiments;
	private int N, T;
	// perform T independent computational experiments on an N-by-N grid
	public PercolationStats(int N, int T) {

		if (N <= 0 || T <= 0) {
			throw new java.lang.IllegalArgumentException("da");
		}
		this.experiments = new double[T];
		this.N = N;
		this.T = T;
		// run experiment
		double count;
		for (int i = 0; i < T; i++) {
			Percolation p = new Percolation(N);
			count = 0;
			int row, col;
			double fraction;
			while (!p.percolates()) {
				row = StdRandom.uniform(1, N+1);
				col = StdRandom.uniform(1, N+1);
				if (!p.isOpen(row, col)) {
					p.open(row, col);
					count++;
				}
			}
			fraction = count / (N * N);
			experiments[i] = fraction;
		}
	}

	public double mean() {

		return StdStats.mean(experiments);
	}

	public double stddev() {

		if (T == 1) {
			return Double.NaN;
		}
		return StdStats.stddev(experiments);

	}

	public double confidenceLo() {

		double mean = mean();
		double stddev = stddev();
		return mean - (1.96 * stddev)/Math.sqrt(T);

	}

	public double confidenceHi() {

		double mean = mean();
		double stddev = stddev();
		return mean + (1.96 * stddev)/Math.sqrt(T);
	}

	public static void main(String[] args) {

		if (args.length < 2) {
			throw new IllegalArgumentException("too few arguments");
		}
		else {
			int N = Integer.parseInt(args[0]);
			int T = Integer.parseInt(args[1]);
			PercolationStats ps = new PercolationStats(N, T);
			System.out.printf("mean\t\t\t = %f\n", ps.mean());
			System.out.printf("stddev\t\t\t = %f\n", ps.stddev());
			System.out.printf("95%% confidence interval\t = %f, %f\n", 
				ps.confidenceLo(), ps.confidenceHi());
		}
	}
}