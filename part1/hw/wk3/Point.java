import java.util.Comparator;

public class Point implements Comparable<Point> {

    // compare points by slope
    public final Comparator<Point> SLOPE_ORDER = new bySlope();       // YOUR DEFINITION HERE

    private final int x;                              // x coordinate
    private final int y;                              // y coordinate

    // create the point (x, y)
    public Point(int x, int y) {
        /* DO NOT MODIFY */
        this.x = x;
        this.y = y;
    }

    private class bySlope implements Comparator<Point> {

        public int compare(Point v, Point w) {

            double slopeV = slopeTo(v);
            double slopeW = slopeTo(w);
            if (slopeV < slopeW) {
                return -1;
            }
            else if (slopeV > slopeW) {
                return 1;
            }
            else {
                return 0;
            }
        }
    }

    // plot this point to standard drawing
    public void draw() {
        /* DO NOT MODIFY */
        StdDraw.point(x, y);
    }

    // draw line between this point and that point to standard drawing
    public void drawTo(Point that) {
        /* DO NOT MODIFY */
        StdDraw.line(this.x, this.y, that.x, that.y);
    }

    // slope between this point and that point
    public double slopeTo(Point that) {
        
        double result;
        if (this.x == that.x && this.y == that.y) {
            result = Double.NEGATIVE_INFINITY;
        }
        else if (this.x == that.x) {
            result = Double.POSITIVE_INFINITY;
        }
        else if (this.y == that.y) {
            result = 0.0;
        }
        else {
            result = (that.y - this.y + 0.0) / (that.x - this.x + 0.0);
        }
        return result;
    }

    // is this point lexicographically smaller than that one?
    // comparing y-coordinates and breaking ties by x-coordinates
    public int compareTo(Point that) {
        
        if (this.y != that.y) {
            return cmp(this.y, that.y);
        }
        else {
            return cmp(this.x, that.x);
        }
    }

    private int cmp(int x, int y) {
        
        if (x < y) {
            return -1;
        }
        else if (x > y) {
            return 1;
        }
        else {
            return 0;
        }
    }

    // return string representation of this point
    public String toString() {
        /* DO NOT MODIFY */
        return "(" + x + ", " + y + ")";
    }

    // unit test
    public static void main(String[] args) {
        /* YOUR CODE HERE */

        Point p = new Point(1, 2);

        //StdOut.print(p.slopeTo(p));
    }
}