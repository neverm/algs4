import java.util.Arrays;

public class Fast {

	private static void printSequence(Point[] points) {

		String out = "";
		for (int i = 0; i < points.length; i++) {
			if (out == "") {
				out += points[i];
			}
			else {
				out += " -> " + points[i];
			}
		}
		System.out.println(out);
		points[0].drawTo(points[points.length-1]);

	}

	public static void main(String[] args) {
		StdDraw.setXscale(0, 32768);
		StdDraw.setYscale(0, 32768);

		String filename = args[0];

		In in = new In(filename);
		int N = Integer.parseInt(in.readLine().trim());

		Point[] points = new Point[N];
		Point[] ordered = new Point[N];
		int index = 0;
		while (!in.isEmpty()) {
			String raw = in.readLine();
			String s[] = raw.trim().split("\\s+");
			if (s.length == 2) {
				int x = Integer.parseInt(s[0]);
				int y = Integer.parseInt(s[1]);
				points[index] = new Point(x, y);
				points[index].draw();
				index++;				
			}
		}

		Arrays.sort(points);	
		// for each point p in array
		// sort array of points by slope to p
		// scan sorted array, look for points with same slope 
		// if found set of points 4 or more - draw, print and stuff

		for (int i = 0; i < points.length; i++) {
			
			Point p = points[i];
			//copy sorted array back
			for (int k = 0; k < points.length; k++) {
				ordered[k] = points[k];
			}
			Arrays.sort(ordered, p.SLOPE_ORDER);
			for (int j = 1; j < ordered.length-1; j++) {
				if (p.slopeTo(ordered[j]) == p.slopeTo(ordered[j+1])) {
					if (p.compareTo(ordered[j]) > 0) {
						continue;	
					}
					int seqStart = j;
					Queue<Point> collinear = new Queue<Point>();
					collinear.enqueue(p);
					collinear.enqueue(ordered[j]);
					while (j < ordered.length-1 && p.slopeTo(ordered[j]) == p.slopeTo(ordered[j+1])) {
						collinear.enqueue(ordered[j+1]);
						j++;
					}

					Point[] sequence = new Point[collinear.size()];
					for (int k = 0; k < sequence.length; k++) {
						sequence[k] = collinear.dequeue();
					}
					boolean isSubsequence = false;
					for (int k = seqStart-1; k >= 0 && p.slopeTo(sequence[1]) == p.slopeTo(ordered[k]); k--) {
						if (p.compareTo(ordered[k]) > 0) {
							//System.out.println("Subseq!");
							isSubsequence = true;
							break;
						}
					}
					if (sequence.length >= 4 && !isSubsequence) {
						printSequence(sequence);
					}
				}
			}
		}
	}
}