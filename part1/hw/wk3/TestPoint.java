import static org.junit.Assert.*;
import java.util.Arrays;

public class TestPoint {

	public void TestSlope() {

		double delta = 0.0001;
		StdOut.println("--- TEST slopeTo ---");

		Point p1 = new Point(1, 1);
		Point p2 = new Point(2, 2);
		Point p3 = new Point(2, 4);
		Point p4 = new Point(1, 4);

		assertEquals(p1.slopeTo(p2), 1.0, delta);
		assertEquals(p1.slopeTo(p3), 3.0, delta);
		assertEquals(p1.slopeTo(p4), Double.POSITIVE_INFINITY, delta);
		assertEquals(p4.slopeTo(p3), 0.0, delta);

		//real data

		Point i = new Point(10000, 0);
		Point j = new Point(0, 10000);
		Point k = new Point(6000, 7000);

		assertNotEquals(i.slopeTo(j), i.slopeTo(k));
	}

	public void TestComparator() {

		double delta = 0.0001;
		StdOut.println("--- TEST slopeTo comparator ---");

		Point p1 = new Point(0, 0);
		Point p2 = new Point(0, 1);
		Point p3 = new Point(1, 1);
		Point p4 = new Point(2, 1);
		Point p5 = new Point(3, 1);

		Point[] ideal = new Point[] {p1, p5, p4, p3, p2};
		Point[] test = new Point[] {p3, p5, p4, p1, p2};
		Arrays.sort(test, p1.SLOPE_ORDER);
		

		Point p1_2 = new Point(0, 0);
		Point[] idealOrder = new Point[] {p1_2, p1};
		Point[] testOrder = new Point[] {p1_2, p1};
		Arrays.sort(testOrder, p2.SLOPE_ORDER);
		assertEquals(idealOrder, testOrder);
	}

	public static  void main(String[] args) {

		StdOut.printf("Start...\n\n");

        TestPoint tester = new TestPoint();

        try {
        	tester.TestSlope();
        	tester.TestComparator();
        }

        catch (Exception e) {
        	StdOut.println(e.toString());
        }

        StdOut.printf("\n\n...finish.");
	}

}