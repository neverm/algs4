import java.util.Arrays;

public class Brute {

	public static void main(String[] args) {
		
		StdDraw.setXscale(0, 32768);
		StdDraw.setYscale(0, 32768);

		String filename = args[0];

		In in = new In(filename);
		int N = Integer.parseInt(in.readLine().trim());

		Point[] ps = new Point[N];
		int index = 0;
		while (!in.isEmpty()) {
			String raw = in.readLine();
			String s[] = raw.trim().split("\\s+");
			if (s.length == 2) {
				int x = Integer.parseInt(s[0]);
				int y = Integer.parseInt(s[1]);
				ps[index] = new Point(x, y);
				ps[index].draw();
				index++;				
			}
		}

		Arrays.sort(ps);

		for (int i = 0; i < ps.length; i++) {
			for (int j = i+1; j < ps.length; j++) {
				double testSlope = ps[i].slopeTo(ps[j]);
				for (int k = j+1; k < ps.length; k++) {
					if (testSlope == ps[i].slopeTo(ps[k])) {
						for (int l = k+1; l < ps.length; l++) {
							if (testSlope == ps[i].slopeTo(ps[l])) {
								// StdOut.printf("%s -> %s -> %s -> %s // %f %f %f %f\n", ps[i], ps[j], ps[k], ps[l], testSlope, ps[i].slopeTo(ps[j]), ps[i].slopeTo(ps[k]), ps[i].slopeTo(ps[l]));
								StdOut.printf("%s -> %s -> %s -> %s\n", ps[i], ps[j], ps[k], ps[l]);
								ps[i].drawTo(ps[l]);
							}
						}
					}
				}
			}
		}

		// for (int i = 0; i < N; i++) {
		// 	StdOut.printf("p%d %s\n", i, ps[i]);
		// }
	}
}